import React from 'react'
import Link from 'next/link'

const link = "underline text-blue-500"

export default function HelpFooter2({
    refer_link,
    href
  }) {
    return (
        <h1 className="mt-8 pt-4 border-t-2 border-black">
            <div className="mb-8">
                Please see our <Link href={href}><a className={link}>{refer_link}</a></Link> for more information about the types of data we gather.
            </div>
          OUR <span className="font-bold">LEGAL POLICIES AND WEB USE GUIDELINES</span> GOVERN
          YOUR USE OF THE FYNSPACE WEBSITE AND THE SERVICE
          WE PROVIDE. Please refer to our: <Link href="/privacy-policy"><a className={link}>Privacy Policy</a></Link>,{' '}
          <Link href="/cookie-policy"><a className={link}>Cookie Policy</a></Link>,{' '}
          <Link href="/ip-publicity-policy"><a className={link}>IP and Publicity Rights Policy</a></Link>,{' '}
          <Link href="/shipping-policy"><a className={link}>Shipping Policy</a></Link>,{' '}
          <Link href="/refund-policy"><a className={link}>Return and Refund Policy</a></Link>,{' '}
          <Link href="/community-guidelines"><a className={link}>Community Guidelines</a></Link> and{' '}
          <Link href="/content-guidelines"><a className={link}>Content Guidelines</a></Link> for relevant information. 
          </h1>
    )
}