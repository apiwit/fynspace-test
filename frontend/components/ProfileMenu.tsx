import * as React from 'react';
import Link from 'next/link';
import { GrDiamond } from 'react-icons/gr';
import { GoMail } from 'react-icons/go';
import { RiTruckLine, RiHistoryFill } from 'react-icons/ri';
import { MdEventNote } from 'react-icons/md';
import { FiHeart } from 'react-icons/fi';
import { BsImages } from 'react-icons/bs';
import { BiImageAdd, BiLock } from 'react-icons/bi';

const menu = 'flex items-center font-semibold mb-2';
const icon = 'mr-2';

const ProfileMenu = () => {
  return (
    <div className="xl:w-1/4 xl:max-h-96 justify-items-start px-8 mt-2 grid gap-3 grid-cols-2 md:grid-cols-4 xl:grid-cols-1">
      <Link href="/inbox">
        <a className={menu}>
          <GoMail className={icon} />
          Inbox
        </a>
      </Link>
      <Link href="/artist/me">
        <a className={menu}>
          <BsImages className={icon} />
          Your Assets
        </a>
      </Link>
      <Link href="/product/add">
        <a className={menu}>
          <BiImageAdd className={icon} />
          Upload Asset
        </a>
      </Link>
      <Link href="/shipment">
        <a className={menu}>
          <RiTruckLine className={icon} />
          Shipping Updates
        </a>
      </Link>
      <Link href="/subscription">
        <a className={menu}>
          <GrDiamond className={icon} />
          Subscription Plan
        </a>
      </Link>
      <Link href="/bid-history">
        <a className={menu}>
          <RiHistoryFill className={icon} />
          Bid History
        </a>
      </Link>
      <Link href="/address">
        <a className={menu}>
          <MdEventNote className={icon} />
          Change Address
        </a>
      </Link>
      <Link href="/wishlist">
        <a className={menu}>
          <FiHeart className={icon} />
          Wishlist
        </a>
      </Link>
      <Link href="/changepassword">
        <a className={menu}>
          <BiLock className={icon} />
          Change Password
        </a>
      </Link>
    </div>
  );
};

export default ProfileMenu;
