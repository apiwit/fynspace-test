import * as React from 'react';

const SoldBadge = () => (
  <div className="absolute top-0 right-0 bg-white rounded-full p-2 m-2 flex items-center justify-center space-x-2 font-bold">
    <img src="/sold.png" alt="Sold" width="25px" />
    <span>SOLD</span>
  </div>
);

export default SoldBadge;
