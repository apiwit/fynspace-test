import { addDays, formatDuration, intervalToDuration } from 'date-fns';
import { useEffect, useState } from 'react';
import { BiTimer } from 'react-icons/bi'

export default function CountDownText({ end }) {
  const [text, setText] = useState('');
  useEffect(() => {
    console.log(`end`, end);
    const interval = setInterval(() => {
      const deadline = new Date(end);
      let timeLeft = intervalToDuration({
        start: new Date(),
        end: deadline,
      });
      setText(
        formatDuration(timeLeft, {
          format: ['hours', 'minutes', 'seconds'],
        })
      );
    }, 1000);
    return () => clearInterval(interval);
  }, []);
  return <p className="flex items-center font-bold"><BiTimer className="w-8 h-8 mr-2"/> {text} Left</p>;
}
