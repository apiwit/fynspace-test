import React from 'react';
import Link from 'next/link';

const Footer = () => {
  return (
    <footer>
      <div className="px-4 bg-gray-100 sm:px-8 py-6 mt-12">
        <ul className="gap-4 flex items-center justify-center">
          <li>
            <Link href="/terms-of-service">
              <a className="ml-2 mr-2">Terms of Service</a>
            </Link>
          </li>
          <li>
            <Link href="/privacy-policy">
              <a className="ml-2 mr-2">Privacy</a>
            </Link>
          </li>
          <li>
            <Link href="/help">
              <a className="ml-2 mr-2">Help</a>
            </Link>
          </li>
        </ul>
      </div>
      <div className="px-4 py-6 bg-black text-white text-center sm:px-8">
        <p>
          Copyright © 2021 DC Globe Co., Ltd. All Rights Reserved. User
          Agreement. Privacy. Cookies. Do not sell my personal information and
          AdChoice
        </p>
      </div>
    </footer>
  );
};

export default Footer;
