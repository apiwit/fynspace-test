import * as React from 'react';
import Logo from './Logo';
import Search from './Search';
import RegisterAndLogin from './RegisterAndLogin';
import Footer from './Footer';

const Layout = (props) => {
  return (
    <>
      <header className="h-24 px-4 sm:px-8">
        <nav className="h-full flex items-center justify-between">
          <Logo />
          <Search />
          <RegisterAndLogin />
        </nav>
      </header>
      {props.children}
      <Footer />
    </>
  );
};

export default Layout;
