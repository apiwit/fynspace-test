import * as React from 'react';
import Link from 'next/link';

const Logo = () => {
  return (
    <>
      <Link href="/">
        <img
          src="/fynspace-logo.png"
          alt="Fynspace"
          width="250px"
          className="cursor-pointer hidden sm:block"
        />
      </Link>
      <Link href="/">
        <img
          src="/fynspace-icon.png"
          alt="Fynspace"
          width="50px"
          className="cursor-pointer sm:hidden"
        />
      </Link>
    </>
  );
};

export default Logo;
