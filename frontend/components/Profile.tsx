import * as React from 'react';
import { useRouter } from 'next/router';
import { useAppContext } from '../providers/AppProvider';
import { useState, useEffect } from 'react';
import { getArtistById } from '../api/auth';
import { BiMenu } from 'react-icons/bi'

const menu = 'flex items-center font-semibold text-center mb-2';
const icon = 'mr-2';

const Profile = () => {
  const { user, isAuthenticated } = useAppContext();
  const router = useRouter();
  const [artist, setArtist] = useState(null);

  useEffect(() => {
    if (user) fetchArtist();
  }, [user]);

  const fetchArtist = async () => {
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  const handleClick = () => {
    if (!isAuthenticated) return router.push('/auth/login');
    else router.push('/main-menu');
  };

  return (
    <div className="relative bg-gray-900 p-8 mt-4 mb-16 sm:mb-12">
      <div className="absolute left-8 lg:left-20 top-4 rounded-full h-24 w-24 flex items-center justify-center bg-gray-300">
        {artist?.image ? (
          <img
            src={artist?.image}
            alt="No Profile Picture"
            className="w-full h-full rounded-full object-cover"
          />
        ) : (
          'No Picture'
        )}
      </div>
      <button
        onClick={handleClick}
        className="flex items-center absolute left-36 lg:left-48 -bottom-8 hover:opacity-70 focus:outline-none"
      >
        FYNSPACE ID: {user?.username ? user?.username : 'Not logged in'} <BiMenu className="ml-2"/>
      </button>
      <span className="flex absolute left-36 sm:left-1/2 -bottom-16 sm:-bottom-8">
        <span className="flex items-center">
          <h5>Following</h5>
          <h5 className="px-2">0</h5>
        </span>
        <span className="flex items-center border-l-2 border-gray-600 px-2">
          <h5>Followers</h5>
          <h5 className="ml-2">0</h5>
        </span>
      </span>
    </div>
  );
};

export default Profile;
