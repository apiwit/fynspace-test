import * as React from 'react';

const InBiddingBadge = () => (
  <div className="absolute top-0 right-0 bg-white rounded-full p-2 m-2 flex items-center justify-center space-x-2 font-bold">
    <img src="/auction.png" alt="Auction" width="25px" />
    <span>In Bidding</span>
  </div>
);

export default InBiddingBadge;
