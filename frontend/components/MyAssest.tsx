import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { getArtistById } from '../api/auth';
import { useAppContext } from '../providers/AppProvider';
import { FiHeart, FiPlus } from 'react-icons/fi';
import { GiCutDiamond } from 'react-icons/gi';
import { BiTimer } from 'react-icons/bi';
import { RiVipCrown2Line } from 'react-icons/ri';
import isBeforeNow from '../helper/date';

export default function MyAssest() {
  const [artist, setArtist] = useState(null);
  const { user } = useAppContext();

  const router = useRouter();
  useEffect(() => {
    if (user) fetchArtist();
  }, [user]);

  const fetchArtist = async () => {
    console.log(`user.id`, user.id);
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  return (
    <div className="mt-16">
      <div className="px-4 flex items-center space-x-2 sm:px-4">
        <GiCutDiamond className="w-6 h-6" />
        <Link href="/artist/me">
          <a className="uppercase font-bold">Your Assets</a>
        </Link>
      </div>
      <div className="px-2 mt-4 grid gap-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        {artist?.products.map(({ id, name, imageUrl, bid_end, like }) => (
          <Link href={`/product/${id}`} key={id}>
            <a className="relative">
              <div className="bg-white rounded-xl shadow-md h-96 relative border border-gray-300">
                <BiTimer className="w-8 h-8 ml-2 my-2" />
                <img
                  src={imageUrl}
                  alt={name}
                  className="w-full h-3/5 object-cover"
                />
                <div className="px-2 py-1">
                  <span className="flex justify-between">
                    <span className="flex items-center">
                      <h5 className="flex text-sm items-center font-bold">
                        <RiVipCrown2Line className="mr-1" />
                        Won By:
                      </h5>
                      <h5 className="text-xs ml-2">01A45H</h5>
                    </span>
                    <span className="flex items-center">
                      <FiHeart className="text-red-500 mr-2" />
                      {like}
                    </span>
                  </span>
                  <div className="font-bold text-right">
                    {bid_end && isBeforeNow(bid_end)
                      ? 'Final Price'
                      : 'Current Price'}
                    <p className="text-green-400">$ 12345</p>
                  </div>
                  <div className="text-sm text-right mt-2">
                    Shipping Status:{' '}
                    {bid_end && isBeforeNow(bid_end) ? 'Bid End' : 'In Biding'}
                  </div>
                </div>
              </div>
            </a>
          </Link>
        ))}
      </div>
    </div>
  );
}
