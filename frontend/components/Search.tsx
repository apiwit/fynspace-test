import * as React from 'react';

const Search = () => {
  return (
    <input
      type="text"
      placeholder="Search"
      className="border border-gray rounded py-1 px-2 hidden lg:block lg:w-2/5 focus:outline-none"
    />
  );
};

export default Search;
