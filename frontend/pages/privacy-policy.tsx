import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { getToS } from '../api/about'
import ReactMarkdown from 'react-markdown'
import HelpFooter2 from '../components/HelpFooter2';

export default function PrivacyPolicy() {
  const [about, setAbout] = useState(null);
  
  const fetchToS = async () => {
    const res = await getToS();
    setAbout(res);
  };

  useEffect(() => {
    fetchToS();
  }, []);

  //In strapi rich text: \n + 2 space for line break
  const privacy = about?.privacy.replace(/\n/gi, '\n &nbsp;');

  return (
    <div>
      <Head>
        <title>Privacy Policy</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <div className="h-32 bg-black"></div>
      <div className="flex justify-center">
        <div className="flex flex-col mt-16 mb-8 w-4/5 xl:w-1/2">
          <h1 className="mb-4 uppercase text-2xl font-bold border-b-2 border-black">
            Privacy Policy
          </h1>
          <ReactMarkdown
            children={privacy}
            className="reactMarkDown"
            />
          <HelpFooter2  
            refer_link="Cookie Policy"
            href="/cookie-policy"
          />
        </div>
      </div>
    </div>
  );
}