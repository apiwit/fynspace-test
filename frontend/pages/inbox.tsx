import 'tailwindcss/tailwind.css';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { RiVipDiamondLine, RiVipCrown2Line } from 'react-icons/ri';
import ProfileMenu from '../components/ProfileMenu';
import Profile from '../components/Profile';
import Wishlist from '../components/Wishlist';
import MyAssest from '../components/MyAssest';

const submitButton =
  'bg-black text-white text-sm font-semibold rounded-lg block transition ease-out duration-300 absolute bottom-4 xl:bottom-8 px-6 py-1 focus:outline-none group-focus:bg-green-200 group-focus:text-black';
const box =
  'group relative flex flex-col items-center rounded-xl bg-white shadow-lg border border-gray-200 p-2 mt-2 w-full pb-16 xl:pb-0 focus:outline-none block transition ease-out duration-300 focus:bg-blue-500';

export default function bidHistory() {
  return (
    <div>
      <Head>
        <title>Inbox</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />

      <div className="flex flex-col xl:flex-row">
        <div className="m-4 flex flex-col xl:w-3/4">
          <div className="flex flex-col xl:flex-row">
            <ProfileMenu />
            <div className="ml-8 mr-8 lg:flex lg:justify-center xl:justify-start xl:w-3/4">
              <div className="relative lg:w-3/4 mt-8 xl:mt-0 flex flex-col">
                <div className={box}>
                  <h5 className="mt-4 font-bold uppercase">Congratulations</h5>
                  <p>You Won</p>
                  <p>20000 USD</p>
                </div>
                <div className={box}>
                  <h5 className="mt-4 font-bold">Recent Bid</h5>
                  <h5 className="mt-4 font-bold uppercase">Congratulations</h5>
                  <p>You Won</p>
                  <p>20000 USD</p>
                  <h5 className="mt-4 font-bold uppercase">Congratulations</h5>
                  <p>You Won</p>
                  <p>20000 USD</p>
                </div>
              </div>

              <div className="relative lg:w-1/4 mt-8 xl:mt-0 lg:ml-4">
                <div className={box}>
                  <span className="flex flex-row items-center">
                    <RiVipDiamondLine className="mt-8 items-center" />
                    <h5 className="mt-4 font-bold items-center">Value of Assets</h5>
                  </span>
                </div>
                <div className={box}>
                  <RiVipDiamondLine className="w-12 h-12 mt-8" />
                  <h5 className="mt-4 font-bold">Fyn Wallet</h5>
                </div>
              </div>
            </div>
          </div>
          <MyAssest />
        </div>
        <Wishlist />
      </div>
    </div>
  );
}
