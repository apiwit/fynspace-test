import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Head from 'next/head'

const submitButton = "border border-black bg-white rounded-lg block transition ease-out duration-300 hover:bg-black hover:text-white absolute -bottom-12 right-0 px-2 py-1";

export default function Help() {
  const [email, setEmail] = useState('');
  const [text, setText] = useState('');
  const router = useRouter();

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(email);
    console.log(text);
    //post
    e.target.reset();
    alert('Your query have been accepted');
  }
  
  return (
    <div>
      <Head>
        <title>Contact Us</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <div className="h-32 bg-black justify-center items-center mt-16 flex items-center">
        <h1 className="text-white text-4xl font-bold text-center">
          Have Trouble Using Fynspace?
        </h1>
      </div>
      <div className="h-96 flex justify-center">
        <div className="flex flex-col mt-16">
          <h1 className="text-2xl font-bold text-center">
            Contact Us Directly via Email
          </h1>
          <form onSubmit={handleSubmit} className="flex flex-col relative">
            <input
              placeholder="Your e-mail"
              type="email"
              className="mt-4 p-2 border-2 rounded-lg border-black focus:outline-none"
              onChange={(e) => setEmail(e.target.value)}
            ></input>
            <textarea
              placeholder="Description of the problems"
              className="max-h-32 mt-4 p-2 border-2 rounded-lg border-black focus:outline-none"
              onChange={(e) => setText(e.target.value)}
            ></textarea>
            <button
              type="submit"
              className={submitButton}>
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
