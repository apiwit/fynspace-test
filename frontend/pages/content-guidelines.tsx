import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { getAbout_3 } from '../api/about'
import ReactMarkdown from 'react-markdown'
import Link from 'next/link'
const link = "underline text-blue-500"

export default function PrivacyPolicy() {
  const [about, setAbout] = useState(null);
  
  const fetchToS = async () => {
    const res = await getAbout_3();
    setAbout(res);
  };

  useEffect(() => {
    fetchToS();
  }, []);

  //In strapi rich text: \n + 2 space for line break
  const content = about?.content.replace(/\n/gi, '\n &nbsp;');

  return (
    <div>
      <Head>
        <title>Content Guidelines</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <div className="h-32 bg-black"></div>
      <div className="flex justify-center">
        <div className="flex flex-col mt-16 mb-8 w-4/5 xl:w-1/2">
          <h1 className="mb-4 uppercase text-2xl font-bold border-b-2 border-black">
            Content Guidelines
          </h1>
          <ReactMarkdown
            children={content}
            className="reactMarkDown"
            />
          <p className="mt-8">For more information, please refer to our{' '}
            <Link href="/help"><a className={link}>Help</a></Link>,{' '}
            <Link href="/community-guidelines"><a className={link}>Community Guidelines</a></Link>{' '}
            and <Link href="/terms-of-service"><a className={link}>Terms of Service</a></Link>{' '}</p>
        </div>
      </div>
    </div>
  );
}