import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import ReactMarkdown from 'react-markdown'
import { getAbout_2 } from '../api/about'
import HelpFooter from '../components/HelpFooter';

export default function IP() {
  const [about, setAbout] = useState(null);
  
  const fetchToS = async () => {
    const res = await getAbout_2();
    setAbout(res);
  };

  useEffect(() => {
    fetchToS();
  }, []);

  //In strapi rich text: \n + 2 space for line break
  const ip = about?.ip.replace(/\n/gi, '\n &nbsp;');

  return (
    <div>
      <Head>
        <title>IP and Publicity Rights Policy</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <div className="h-32 bg-black"></div>
      <div className="flex justify-center">
        <div className="flex flex-col mt-16 mb-8 w-4/5 xl:w-1/2">
          <h1 className="mb-4 uppercase text-2xl font-bold border-b-2 border-black">
            IP AND PUBLICITY RIGHTS POLICY
          </h1>
          <ReactMarkdown
            children={ip}
            className="reactMarkDown"
            />
          <HelpFooter />
        </div>
      </div>
    </div>
  );
}