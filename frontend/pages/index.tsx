import Head from 'next/head';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { getProducts } from '../api/product';
import { FiSearch } from 'react-icons/fi';
import InBiddingBadge from '../components/InBiddingBadge';
import { IoMdHeart } from 'react-icons/io';
import isBeforeNow from '../helper/date'
import SoldBadge from '../components/SoldBadge';

export default function Home() {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    const products = await getProducts();
    setProducts(products);
  };

  return (
    <div>
      <Head>
        <title>Fynspace</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <main className="2xl:container 2xl:mx-auto">
        {/* <div className="grid gap-3 md:grid-cols-2 md:gap-3">
          <div className="p-8 h-48 flex items-center bg-homepage-left bg-cover bg-center bg-no-repeat font-bold">
            <div>
              <h1 className="text-white uppercase text-xl">
                <span className="block">Find your art</span>
                <span className="block">share your part</span>
              </h1>
              <button className="text-white border border-white px-5 py-1 font-bold mt-3 transition ease-out duration-300 focus:outline-none hover:bg-white hover:text-black">
                Sell with us
              </button>
            </div>
          </div>
          <div className="p-8 h-48 flex items-center bg-homepage-right bg-cover bg-center bg-no-repeat font-bold">
            <div>
              <h1 className="text-white uppercase text-xl">
                <span className="block">Let's get together and</span>
                <span className="block">show your inspiration</span>
              </h1>
              <button className="text-white border border-white px-5 py-1 font-bold mt-3 transition ease-out duration-300 focus:outline-none hover:bg-white hover:text-black">
                Subscribe now
              </button>
            </div>
          </div>
        </div> */}

        <div className="mx-8 mt-2">
          <a
            href="#"
            className="text-2xl uppercase flex items-center space-x-2"
          >
            <FiSearch />
            <span>Explore</span>
          </a>
        </div>
        <div className="px-8 mt-2 grid gap-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
          {products.map(({ id, name, imageUrl, bid_end }) => (
            <Link href={`/product/${id}`} key={id}>
              <a className="relative">
                <img
                  src={imageUrl}
                  alt={name}
                  className="w-full h-96 object-cover"
                />
                {bid_end &&
                  (isBeforeNow(bid_end) ? (
                    <SoldBadge />
                  ) : (
                    <InBiddingBadge />
                  ))}
                <div className="absolute top-0 w-full h-96 flex items-center justify-center">
                  <IoMdHeart className="w-20 h-20 text-white" />
                </div>
              </a>
            </Link>
          ))}
        </div>
      </main>
    </div>
  );
}
