import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link'
import ReactMarkdown from 'react-markdown'
import { getAbout_2 } from '../api/about'
import HelpFooter2 from '../components/HelpFooter2';

export default function CookiePolicy() {
  const [about, setAbout] = useState(null);
  
  const fetchToS = async () => {
    const res = await getAbout_2();
    setAbout(res);
  };

  useEffect(() => {
    fetchToS();
  }, []);

  //In strapi rich text: \n + 2 space for line break
  const cookie = about?.cookie.replace(/\n/gi, '\n &nbsp;');

  return (
    <div>
      <Head>
        <title>Cookie Policy</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <div className="h-32 bg-black"></div>
      <div className="flex justify-center">
        <div className="flex flex-col mt-16 mb-8 w-4/5 xl:w-1/2">
          <h1 className="mb-4 uppercase text-2xl font-bold border-b-2 border-black">
            Cookie Policy
          </h1>
          <ReactMarkdown
            children={cookie}
            className="reactMarkDown"
            />
          <HelpFooter2 
            refer_link="Privacy Policy"
            href="/privacy-policy"
            />
        </div>
      </div>
    </div>
  );
}