import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { BiTimer } from 'react-icons/bi';
import { FiHeart } from 'react-icons/fi';
import { RiVipCrown2Line } from 'react-icons/ri';
import { getArtistById } from '../../api/auth';
import Profile from '../../components/Profile';
import isBeforeNow from '../../helper/date';
import { useAppContext } from '../../providers/AppProvider';

const follow_b =
  'ml-4 border border-blue-600 rounded-lg focus:outline-none text-blue-800 w-24 xl:w-36 px-2 py-1';
const unfollow_b =
  'ml-4 border border-red-400 rounded-lg focus:outline-none text-red-500 w-24 xl:w-36 px-2 py-1';

export default function Help() {
  const { isAuthenticated } = useAppContext();
  const [artist, setArtist] = useState(null);
  const [follow, setFollow] = useState(false);

  const router = useRouter();
  useEffect(() => {
    fetchArtist();
  }, [router.query]);

  const fetchArtist = async () => {
    if (Object.keys(router.query).length == 0) return; // query is still loading
    const { artistId } = router.query;
    const result = await getArtistById(artistId);
    setArtist(result);
  };

  const handleFollow = () => {
    //if (!isAuthenticated) return router.push('/auth/login');
    if (!isAuthenticated) return alert('Please log in to use this feature');
    else return setFollow(!follow);
  };

  return (
    <div>
      <Head>
        <title>{artist?.username} Profile</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />
      <div className="pt-8 px-4 flex flex-col md:flex-row justify-between font-semibold">
        <span className="text-sm">
          <h1>You are currently viewing {artist?.username} profile page</h1>
          <h3 className="font-normal">
            This user joined: {artist?.created_at}
          </h3>
        </span>
        <span className="mt-8 md:mt-0 flex justify-between items-center">
          {follow ? 'Click to unfollow this user' : 'Click to follow this user'}
          <button
            onClick={handleFollow}
            className={follow ? unfollow_b : follow_b}
          >
            {follow ? 'Unfollow' : 'Follow'}
          </button>
        </span>
      </div>

      <div className="px-4 mt-4 grid gap-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        {artist?.products.map(({ id, name, imageUrl, bid_end, like }) => (
          <Link href={`/product/${id}`} key={id}>
            <a className="relative">
              <div className="bg-white rounded-xl shadow-md h-96 relative border border-gray-200">
                <BiTimer className="w-8 h-8 ml-2 my-2" />
                <img
                  src={imageUrl}
                  alt={name}
                  className="w-full h-3/5 object-cover"
                />
                <div className="px-2 py-1">
                  <span className="flex justify-between">
                    <span className="flex items-center">
                      <h5 className="flex text-sm items-center font-bold">
                        <RiVipCrown2Line className="mr-1" />
                        Won By:
                      </h5>
                      <h5 className="text-xs ml-2">01A45H</h5>
                    </span>
                    <span className="flex items-center">
                      <FiHeart className="text-red-500 mr-2" />
                      {like ? like : 0}
                    </span>
                  </span>
                  <div className="font-bold text-right">
                    {bid_end && isBeforeNow(bid_end)
                      ? 'Final Price'
                      : 'Current Price'}
                    <p className="text-green-400">$ 12345</p>
                  </div>
                  <div className="text-sm text-right mt-2">
                    Shipping Status:{' '}
                    {bid_end && isBeforeNow(bid_end) ? 'Bid End' : 'In Biding'}
                  </div>
                </div>
              </div>
            </a>
          </Link>
        ))}
      </div>
    </div>
  );
}
