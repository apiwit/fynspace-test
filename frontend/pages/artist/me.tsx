import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { getArtistById } from '../../api/auth';
import { useAppContext } from '../../providers/AppProvider';
import { FiHeart, FiPlus } from 'react-icons/fi';
import { GiCutDiamond } from 'react-icons/gi';
import Profile from '../../components/Profile';
import Head from 'next/head';
import Wishlist from '../../components/Wishlist';
import { BiTimer } from 'react-icons/bi';
import isBeforeNow from '../../helper/date';
import { RiVipCrown2Line } from 'react-icons/ri';

export default function me() {
  const [artist, setArtist] = useState(null);
  const { user } = useAppContext();

  const router = useRouter();
  useEffect(() => {
    if (user) fetchArtist();
  }, [user]);

  const fetchArtist = async () => {
    console.log(`user.id`, user.id);
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  return (
    <div>
      <Head>
        <title>Your Assets</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />
      <div className="xl:flex">
        <div className="xl:w-3/4">
          <div className="pt-8 px-4 flex items-center space-x-2 sm:px-8">
            <GiCutDiamond className="w-6 h-6" />
            <h1 className="uppercase font-bold">Your Assets</h1>
          </div>
          <div className="px-4 xl:px-8 mt-2 grid gap-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
            {artist?.products.map(({ id, name, imageUrl, bid_end, like }) => (
              <Link href={`/product/${id}`} key={id}>
                <a className="relative">
                  <div className="bg-white rounded-xl shadow-md h-96 relative">
                    <BiTimer className="w-8 h-8 ml-2 my-2" />
                    <img
                      src={imageUrl}
                      alt={name}
                      className="w-full h-3/5 object-cover"
                    />
                    <div className="px-2 py-1">
                      <span className="flex justify-between">
                        <span className="flex items-center">
                          <h5 className="flex text-sm items-center font-bold">
                            <RiVipCrown2Line className="mr-1"/>
                            Won By:
                          </h5>
                          <h5 className="text-xs ml-2">01A45H</h5>
                        </span>
                        <span className="flex items-center">
                          <FiHeart className="text-red-500 mr-2" />
                          {like? like : 0}
                        </span>
                      </span>
                      <div className="font-bold text-right">
                        {bid_end && isBeforeNow(bid_end)
                          ? 'Final Price'
                          : 'Current Price'}
                        <p className="text-green-400">$ 12345</p>
                      </div>
                      <div className="text-sm text-right mt-2">
                        Shipping Status:{' '}
                        {bid_end && isBeforeNow(bid_end)
                          ? 'Bid End'
                          : 'In Biding'}
                      </div>
                    </div>
                  </div>
                </a>
              </Link>
            ))}
            <Link href="/product/add">
              <a className="h-96 mb-24 relative w-full bg-gray-200 flex justify-center items-center transition ease-out duration-300 hover:opacity-70">
                <FiPlus className="w-10 h-10" />
              </a>
            </Link>
          </div>
        </div>
        <Wishlist />
      </div>
    </div>
  );
}
