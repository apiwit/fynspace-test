import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useAppContext } from '../../providers/AppProvider';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export default function forgotpassword() {
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmitEmail = () => {
    setMessage('processing...');
    axios
      .post(`${API_URL}/auth/forgot-password`, {
        email, // user's email
      })
      .then((response) => {
        setMessage('Your user received an email');
      })
      .catch((error) => {
        setMessage(`An error occurred: ${error.response}`);
      });
  };
  // Request API.

  return (
    <div>
      Your email:
      <input
        className="border"
        type="text"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
      ></input>
      <button className="border bg-gray-200" onClick={handleSubmitEmail}>
        Reset
      </button>
      {message}
    </div>
  );
}
