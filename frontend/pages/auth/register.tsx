import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { registerUser } from '../../api/auth';
import { useAppContext } from '../../providers/AppProvider';
import { FaFacebookF } from 'react-icons/fa';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';
const formInput = 'border w-full px-2 py-1 focus:outline-none';

export default function register() {
  const router = useRouter();

  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [email, setEmail] = useState('');

  const [loading, setLoading] = useState(false);
  const { user, setUser } = useAppContext();

  const handleRegister = async (e: React.FormEvent) => {
    e.preventDefault();
    setLoading(true);
    try {
      const user = await registerUser(email, password);
      // set authed user in global context object
      console.log(`user`, user);
      setUser(user);
    } catch (error) {
      console.error(error);
    }

    setLoading(false);
    router.push('/');
  };

  const singupWithFacebook = () => {
    router.push(`${API_URL}/connect/facebook`);
  };

  const singupWithGoogle = () => {
    router.push(`${API_URL}/connect/google`);
  };

  return (
    <div className="my-6 flex justify-center lg:mb-40 2xl:container 2xl:mx-auto">
      <div className="w-4/5 md:w-3/5">
        <div className="justify-between lg:flex items-center">
          <h1 className="text-2xl text-gray-700">
            Create your Fynspace Account
          </h1>
          <p className="text-sm text-gray-500">
            Already member?{' '}
            <span className="text-blue-600 hover:underline">
              <Link href="/auth/login">Sign In</Link>
            </span>{' '}
            here.
          </p>
        </div>
        <form
          className="border p-8 mt-6 shadow-md grid lg:grid-cols-2"
          onSubmit={handleRegister}
        >
          <div className="space-y-4 lg:pr-8">
            <div>
              <label className="block">Email</label>
              <input
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                className={formInput}
              />
            </div>
            <div>
              <label className="block">Password</label>
              <input
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type="password"
                className={formInput}
              />
            </div>
            <div>
              <label className="block">Confirm Password</label>
              <input
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                type="password"
                className={formInput}
              />
            </div>
          </div>
          <div className="flex flex-col justify-center mt-6 lg:mt-0 lg:border-l lg:pl-8">
            <div>
              <button
                type="submit"
                className="p-1 border border-black bg-white rounded mx-auto block w-full transition ease-out duration-300 hover:bg-black hover:text-white"
              >
                {loading ? 'Loading..' : 'Sign Up'}
              </button>
              <p className="my-4 text-sm text-gray-500">Or Sign Up with</p>
              <button
                className="w-full p-1 border border-facebook rounded bg-facebook text-white flex items-center justify-center space-x-2"
                onClick={singupWithFacebook}
              >
                <FaFacebookF />
                <span>Facebook</span>
              </button>
              <button
                className="mt-2 w-full p-1 border border-gray-500 rounded bg-white text-gray-700 flex items-center justify-center space-x-2"
                onClick={singupWithGoogle}
              >
                <img width="20" height="20" src="/g-logo.png" />
                <span>Google</span>
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
