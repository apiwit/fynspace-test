import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Router, useRouter } from 'next/router';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export default function resetpassword() {
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  const router = useRouter();

  const handleSubmit = () => {
    const parsedUrl = new URL(window.location.href);
    const code = parsedUrl.searchParams.get('code');
    setLoading(true);
    axios
      .post(`${API_URL}/auth/reset-password`, {
        code,
        password,
        passwordConfirmation,
      })
      .then((response) => {
        router.push('/auth/login');
      })
      .catch((error) => {
        setError(error.response);
      });
  };
  // Request API.

  return (
    <div>
      <div>
        New password:
        <input
          className="border"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        ></input>
      </div>
      <div>
        New password:
        <input
          className="border"
          type="password"
          value={passwordConfirmation}
          onChange={(e) => setPasswordConfirmation(e.target.value)}
        ></input>
      </div>
      <button className="border bg-gray-200" onClick={handleSubmit}>
        Submit
      </button>
      {error && error}
      {loading && 'Processing....'}
    </div>
  );
}
