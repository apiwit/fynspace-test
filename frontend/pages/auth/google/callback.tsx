import axios from 'axios';
import React, { useEffect } from 'react';
import { useAppContext } from '../../../providers/AppProvider';
import Cookie from 'js-cookie';
import { useRouter } from 'next/router';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export default function callback() {
  const { setUser } = useAppContext();
  const router = useRouter();

  useEffect(() => {
    login();
  }, []);

  const login = async () => {
    const parsedUrl = new URL(window.location.href);
    const access_token = parsedUrl.searchParams.get('access_token');

    const response = await axios.get(`${API_URL}/auth/google/callback`, {
      params: { access_token },
    });
    Cookie.set('token', response.data.jwt);
    setUser(response.data.user);
    router.push('/');
  };

  return <div>Connecting you via google</div>;
}
