import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { loginUser } from '../../api/auth';
import { useAppContext } from '../../providers/AppProvider';
import { FaFacebookF } from 'react-icons/fa';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';
const formInput = 'border w-full px-2 py-1 focus:outline-none';

export default function login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const router = useRouter();

  const [loading, setLoading] = useState(false);
  const { user, setUser, isAuthenticated } = useAppContext();

  const handleLogin = async (e: React.FormEvent) => {
    e.preventDefault();
    setLoading(true);
    try {
      const user = await loginUser(email, password);
      // set authed user in global context object
      console.log(`user`, user);
      setUser(user);
    } catch (error) {
      console.error(error);
    }
    setLoading(false);
    router.push('/');
  };

  const loginWithFacebook = () => {
    router.push(`${API_URL}/connect/facebook`);
  };

  useEffect(() => {
    if (isAuthenticated) {
      router.push('/'); // redirect if you're already logged in
    }
  }, []);

  return (
    <div className="my-6 flex justify-center lg:mb-48 2xl:container 2xl:mx-auto">
      <div className="w-4/5 md:w-3/5">
        <div className="justify-between lg:flex items-center">
          <h1 className="text-2xl text-gray-700">
            Welcome to Fynspace! Please sign in.
          </h1>
          <p className="text-sm text-gray-500">
            No account?{' '}
            <span className="text-blue-600 hover:underline">
              <Link href="/auth/register">Sign Up</Link>
            </span>{' '}
            here.
          </p>
        </div>
        <form
          className="border p-8 mt-6 shadow-md grid lg:grid-cols-2"
          onSubmit={handleLogin}
        >
          <div className="space-y-4 lg:pr-8">
            <div>
              <label className="block">Email</label>
              <input
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                type="email"
                className={formInput}
              />
            </div>
            <div>
              <label className="block">Password</label>
              <input
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type="password"
                className={formInput}
              />
            </div>
            <div className="text-right text-sm text-blue-600 hover:underline">
              <Link href={'/auth/forgotpassword'}>
                <a>Forgot Password</a>
              </Link>
            </div>
          </div>
          <div className="flex flex-col justify-center mt-6 lg:mt-0 lg:border-l lg:pl-8">
            <button
              className="p-1 border border-black bg-white rounded mx-auto block w-full transition ease-out duration-300 hover:bg-black hover:text-white"
              type="submit"
            >
              {loading ? 'Loading..' : 'Sign In'}
            </button>
            <p className="my-4 text-sm text-gray-500">Or Sign In with</p>
            <button
              className="w-full p-1 border border-facebook rounded bg-facebook text-white flex items-center justify-center space-x-2"
              onClick={loginWithFacebook}
            >
              <FaFacebookF />
              <span>Facebook</span>
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
