import 'tailwindcss/tailwind.css';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { RiVipDiamondLine, RiVipCrown2Line } from 'react-icons/ri';
import ProfileMenu from '../components/ProfileMenu';
import Profile from '../components/Profile';
import Wishlist from '../components/Wishlist';
import MyAssest from '../components/MyAssest';

const submitButton =
  'bg-black text-white text-sm font-semibold rounded-lg block transition ease-out duration-300 absolute bottom-4 xl:bottom-8 px-6 py-1 focus:outline-none group-focus:bg-green-200 group-focus:text-black';
const box =
  'group relative flex flex-col items-center rounded-xl bg-white shadow-lg border border-gray-200 p-2 mt-2 min-w-full pb-16 xl:pb-0 focus:outline-none block transition ease-out duration-300 focus:bg-blue-500';

export default function Subscription() {
  return (
    <div>
      <Head>
        <title>Subscription Plan</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />

      <div className="flex flex-col xl:flex-row">
        <div className="m-4 flex flex-col xl:w-3/4">
          <div className="flex flex-col xl:flex-row">
            <ProfileMenu />
            <div className="ml-8 mr-8 lg:flex lg:justify-center xl:justify-start xl:w-2/3">
              <div className="relative lg:w-3/4 xl:w-full">
                <div className="grid gap-4 grid-cols-1 md:grid-cols-3 h-full">
                  <button className={box}>
                    <RiVipDiamondLine className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      BEGINNER
                    </h5>
                    <h5 className="mt-2 font-bold text-xl text-blue-600 group-focus:text-white">
                      3 $ / Month
                    </h5>
                    <h5 className="mt-8 font-semibold text-sm group-focus:text-white">
                      Recurs For 1 Month
                    </h5>
                    <h5 className="font-semibold text-sm group-focus:text-white">
                      Up to{' '}
                      <span className="text-blue-600 group-focus:text-white">
                        15 Assests
                      </span>
                    </h5>
                    <button className={submitButton}>SELECT</button>
                  </button>
                  <button className={box}>
                    <span className="flex flex-row group-focus:text-white">
                      <RiVipDiamondLine className="w-12 h-12 mt-8" />
                      <RiVipDiamondLine className="w-12 h-12 mt-8" />
                    </span>
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      PRO
                    </h5>
                    <h5 className="mt-2 font-bold text-xl text-blue-600 group-focus:text-white">
                      40 $ / Year
                    </h5>
                    <h5 className="mt-8 font-semibold text-sm group-focus:text-white">
                      Recurs For 1 Year
                    </h5>
                    <h5 className="font-semibold text-sm group-focus:text-white">
                      Up to{' '}
                      <span className="text-blue-600 group-focus:text-white">
                        30 Assests
                      </span>
                    </h5>
                    <button className={submitButton}>SELECT</button>
                  </button>
                  <button className={box}>
                    <RiVipCrown2Line className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      VIP
                    </h5>
                    <h5 className="mt-2 font-bold text-xl text-blue-600 group-focus:text-white">
                      100 $ / 3 Years
                    </h5>
                    <h5 className="mt-8 font-semibold text-sm group-focus:text-white">
                      Recurs For 3 Years
                    </h5>
                    <h5 className="font-semibold text-sm text-blue-600 group-focus:text-white">
                      Unlimited Assets
                    </h5>
                    <h5 className="font-semibold text-sm text-blue-600 group-focus:text-white">
                      Multimedia Functions
                    </h5>

                    <button className={submitButton}>SELECT</button>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <MyAssest />
        </div>
        <Wishlist />
      </div>
    </div>
  );
}
