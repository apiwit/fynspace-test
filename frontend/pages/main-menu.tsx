import * as React from 'react';
import Link from 'next/link';
import { RiVipDiamondLine } from 'react-icons/ri';
import { GoMail } from 'react-icons/go';
import { RiTruckLine, RiHistoryFill } from 'react-icons/ri';
import { MdEventNote } from 'react-icons/md';
import { FiHeart } from 'react-icons/fi';
import { BsImages } from 'react-icons/bs';
import { BiImageAdd, BiLock } from 'react-icons/bi';
import Profile from '../components/Profile';
import Head from 'next/head';

const menu =
  'border-2 border-gray-400 hover:border-black rounded-xl p-4 hover:text-blue-700';
const menu_text = 'flex flex-col md:flex-row md:items-center font-semibold text-lg mb-2';
const desc = 'hidden text-gray-600 font-medium';
const icon = 'mr-2';

const Menu = () => {
  return (
    <div>
      <Head>
        <title>Fynspace</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />
      <div className="flex justify-center">
        <div className="flex px-8 mt-12 mb-12 grid gap-3 grid-cols-2 lg:grid-cols-3 xl:w-3/4">
          <Link href="/inbox">
            <a className={menu}>
              <div className={menu_text}>
                <GoMail className={icon} />
                Inbox
              </div>
              <p className={desc}>
                Check what's going on in your account
              </p>
            </a>
          </Link>
          <Link href="/artist/me">
            <a className={menu}>
              <div className={menu_text}>
                <BsImages className={icon} />
                Your Assets
              </div>
              <p className={desc}>
                View all of your assets that have been upload on this website
              </p>
            </a>
          </Link>
          <Link href="/product/add">
            <a className={menu}>
              <div className={menu_text}>
                <BiImageAdd className={icon} />
                Upload Asset
              </div>
              <p className={desc}>
                Upload new asset. Spreading your though and imagination with
                art!
              </p>
            </a>
          </Link>
          <Link href="/shipment">
            <a className={menu}>
              <div className={menu_text}>
                <RiTruckLine className={icon} />
                Shipping Updates
              </div>
              <p className={desc}>
                View all of your assets that will be shipping. Update track
                number and mark an item as shipped
              </p>
            </a>
          </Link>
          <Link href="/subscription">
            <a className={menu}>
              <div className={menu_text}>
                <RiVipDiamondLine className={icon} />
                Subscription Plan
              </div>
              <p className={desc}>
                You can view and change your subscription plan to match your
                preference
              </p>
            </a>
          </Link>
          <Link href="/bid-history">
            <a className={menu}>
              <div className={menu_text}>
                <RiHistoryFill className={icon} />
                Bid History
              </div>
              <p className={desc}>View your bidding history</p>
            </a>
          </Link>
          <Link href="/address">
            <a className={menu}>
              <div className={menu_text}>
                <MdEventNote className={icon} />
                Change Address
              </div>
              <p className={desc}>
                Update your address that will be used for billing and shipping
              </p>
            </a>
          </Link>
          <Link href="/wishlist">
            <a className={menu}>
              <div className={menu_text}>
                <FiHeart className={icon} />
                Wishlist
              </div>
              <p className={desc}>View your wishlist. Mabye it's time to get what you want!</p>
            </a>
          </Link>
          <Link href="/changepassword">
            <a className={menu}>
              <div className={menu_text}>
                <BiLock className={icon} />
                Change Password
              </div>
              <p className={desc}>Changing your password</p>
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Menu;
