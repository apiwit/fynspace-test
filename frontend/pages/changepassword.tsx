import 'tailwindcss/tailwind.css';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
//import { changePassword } from '../api/auth';
import { useAppContext } from '../providers/AppProvider';
import Head from 'next/head';
import ProfileMenu from '../components/ProfileMenu';
import Profile from '../components/Profile'
import Wishlist from '../components/Wishlist';
import MyAssest from '../components/MyAssest';
import axios from 'axios';
import { FormEvent } from 'react';

const submitButton =
  'border border-black bg-white rounded-lg block transition ease-out duration-300 hover:bg-black hover:text-white absolute -bottom-12 right-0 xl:bottom-28 px-2 py-1 focus:outline-none';
const input_style =
  'mt-4 mb-4 p-2 border-2 rounded-lg border-black focus:outline-none';

export default function ChangePassword() {
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const router = useRouter();
  const { user } = useAppContext();

  const handleSubmit = async (e: React.FormEvent) => {
    alert('Function Under Development')
  };

  return (
    <div>
      <Head>
        <title>Change Password</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />

      <div className="flex flex-col xl:flex-row">
        <div className="m-4 flex flex-col xl:w-3/4">
          <div className="flex flex-col xl:flex-row">
            <ProfileMenu />
            <div className="ml-8 mr-8 lg:flex lg:justify-center xl:justify-start xl:w-2/3">
              <form onSubmit={handleSubmit} className="relative lg:w-3/4">
                <h1 className="font-bold mt-8 xl:mt-2 mb-4">Change Password</h1>
                <div className={input_style}>
                  <input
                    placeholder="Current Password"
                    className="min-w-full focus:outline-none"
                    required
                    value={oldPassword}
                    onChange={(e) => setOldPassword(e.target.value)}
                    type="password"
                  />
                </div>
                <div className={input_style}>
                  <input
                    placeholder="New Password"
                    className="min-w-full focus:outline-none"
                    required
                    value={newPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                    type="password"
                  />
                </div>
                <div className={input_style}>
                  <input
                    placeholder="Confirm Password"
                    className="min-w-full focus:outline-none"
                    required
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    type="password"
                  />
                </div>
                <div>
                  <button className={submitButton} type="submit">
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
          <MyAssest />
        </div>
        <Wishlist />
      </div>
    </div>
  );
}
