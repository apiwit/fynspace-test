import 'tailwindcss/tailwind.css';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { RiVipDiamondLine, RiVipCrown2Line } from 'react-icons/ri';
import ProfileMenu from '../components/ProfileMenu';
import Profile from '../components/Profile';
import Wishlist from '../components/Wishlist';
import MyAssest from '../components/MyAssest';

const submitButton =
  'bg-black text-white text-sm font-semibold rounded-lg block transition ease-out duration-300 absolute bottom-4 xl:bottom-8 px-6 py-1 focus:outline-none group-focus:bg-green-200 group-focus:text-black';
const box =
  'group relative flex flex-col items-center rounded-xl bg-white shadow-lg border border-gray-200 p-2 mt-2 min-w-full pb-16 xl:pb-0 focus:outline-none block transition ease-out duration-300 focus:bg-blue-500';

export default function bidHistory() {
  return (
    <div>
      <Head>
        <title>Bid History</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />

      <div className="flex flex-col xl:flex-row">
        <div className="m-4 flex flex-col xl:w-3/4">
          <div className="flex flex-col xl:flex-row">
            <ProfileMenu />
            <div className="ml-8 mr-8 lg:flex lg:justify-center xl:justify-start xl:w-2/3">
              <div className="relative lg:w-3/4 mt-8 xl:mt-0">
                <h5 className="font-bold">Bid History</h5>
                <div className="grid gap-4 sm:grid-cols-2 md:grid-cols-3">
                  <button className={box}>
                    <RiVipDiamondLine className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Bid 1
                    </h5>
                  </button>
                  <button className={box}>
                    <span className="flex flex-row group-focus:text-white">
                      <RiVipDiamondLine className="w-12 h-12 mt-8" />
                    </span>
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Bid 2
                    </h5>
                  </button>
                  <button className={box}>
                    <RiVipCrown2Line className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Bid 3
                    </h5>
                  </button>
                  <button className={box}>
                    <RiVipCrown2Line className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Bid 4
                    </h5>
                  </button>
                </div>
              </div>
              
              <div className="relative lg:w-1/4 mt-8 xl:mt-0 lg:ml-12">
                <h5 className="font-bold">Recent Bid</h5>
                <div className="grid gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-1">
                  <button className={box}>
                    <RiVipDiamondLine className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Recent 1
                    </h5>
                  </button>
                  <button className={box}>
                    <span className="flex flex-row group-focus:text-white">
                      <RiVipDiamondLine className="w-12 h-12 mt-8" />
                    </span>
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Recent 2
                    </h5>
                  </button>
                  <button className={box}>
                    <RiVipCrown2Line className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Recent 3
                    </h5>
                  </button>
                  <button className={box}>
                    <RiVipCrown2Line className="w-12 h-12 mt-8 group-focus:text-white" />
                    <h5 className="mt-4 font-bold text-xl group-focus:text-white">
                      Recent 4
                    </h5>
                  </button>
                </div>
              </div>

            </div>
          </div>
          <MyAssest />
        </div>
        <Wishlist />
      </div>
    </div>
  );
}
