import { useRouter } from 'next/router';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';
import { addProduct, getTechniques } from '../../api/product';
import { uploadFile } from '../../api/upload';
import { useAppContext } from '../../providers/AppProvider';
import { MdClose } from 'react-icons/md';
import { GoGear } from 'react-icons/go';
import { AiOutlineHome, AiOutlineGlobal } from 'react-icons/ai';
import { FaTruck } from 'react-icons/fa';
import DatePicker from 'react-datepicker';
import ReactFlagsSelect from 'react-flags-select';
import 'react-datepicker/dist/react-datepicker.css';
import Profile from '../../components/Profile';
import Modal from '../../components/Modal';
import { getArtistById } from '../../api/auth';
import ReactCountryFlag from 'react-country-flag';
import { addShippingAddress, UpdateShippingAddress } from '../../api/address';
import { addBankAccount, UpdateBankAccount } from '../../api/bank';

const button =
  'border border-black bg-black text-white px-3 py-1 my-2 transition ease-out duration-300 hover:bg-white hover:text-black focus:outline-none';
const addr_box =
  'relative rounded-xl bg-white shadow-lg border border-gray-200 p-2 mt-2 min-w-full focus:outline-none';
const input_div = 'flex justify-between flex-col sm:flex-row';
const input_style =
  'my-1 p-1 border-2 rounded-lg border-black focus:outline-none max-h-32 sm:w-1/2';

export default function add() {
  const router = useRouter();
  const { user, isAuthenticated } = useAppContext();
  const [showModal, setShowModal] = React.useState(false);
  const [showBankModal, setShowBankModal] = React.useState(false);
  const [artist, setArtist] = useState(null);

  const [loading, setLoading] = useState(false);

  //Prouct
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [file, setFile] = useState(null);
  const [price, setPrice] = useState(0.00);
  const [weight, setWeight] = useState(0.00);
  const [length, setLength] = useState(0.00);
  const [width, setWidth] = useState(0.00);
  const [height, setHeight] = useState(0.00);
  const [techniques, setTechniques] = useState([]);
  const [technique, setTechnique] = useState(1);
  const [bidEnd, setBidEnd] = useState(new Date());
  const [bidEndTime, setBidEndTime] = useState(new Date());
  const [artistName, setArtistName] = useState('');

  //Address Modal
  const [address_number, setAddress_number] = useState(artist?.shipping_address?.address_number);
  const [office_name, setOffice_name] = useState(artist?.shipping_address?.office_name);
  const [road, setRoad] = useState(artist?.shipping_address?.road);
  const [sub_district, setSub_district] = useState(artist?.shipping_address?.sub_district);
  const [district, setDistrict] = useState(artist?.shipping_address?.district);
  const [province, setProvince] = useState(artist?.shipping_address?.province);
  const [zip_code, setZip_code] = useState(artist?.shipping_address?.zip_code);
  const [country, setCountry] = useState(artist?.shipping_address?.country);
  const [bank_name, setBankName] = useState(artist?.bank?.name);
  const [bank_number, setBankNumber] = useState(artist?.bank?.number);

  useEffect(() => {
    if (user) fetchArtist();
  }, [user]);

  const fetchArtist = async () => {
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  const handleClickAddAdress = () => {
    if (!isAuthenticated) return router.push('/auth/login');
    setShowModal(true);
  };

  const handleClickAddBank = () => {
    if (!isAuthenticated) return router.push('/auth/login');
    setShowBankModal(true);
  };

  const onSubmitAddress = async (e) => {
    //e.preventDefault();
    if (!artist?.shipping_address?.id) {
      console.log('add')
      await addShippingAddress({
        address_number,
        office_name,
        road,
        sub_district,
        district,
        province,
        zip_code,
        country,
        owner: user.id,
      });
    } else {
      console.log('update');
      console.log(address_number,
        office_name,
        road,
        sub_district,
        district,
        province,
        zip_code,
        country,
        artist?.shipping_address?.id
      )
      await UpdateShippingAddress(
        address_number,
        office_name,
        road,
        sub_district,
        district,
        province,
        zip_code,
        country,
        artist?.shipping_address?.id
      );
    }
    setShowModal(false);
    fetchArtist();
  };

  const onSubmitBank = async (e) => {
    //e.preventDefault();
    console.log(artist?.bank);
    console.log(bank_name, bank_number);
    if (!artist?.bank?.id) {
      console.log('add')
      await addBankAccount({
        name: bank_name,
        number: bank_number,
        owner: user.id,
      });
    } else {
      console.log('update');
      await UpdateBankAccount(
        bank_name ? bank_name : artist?.bank?.name,
        bank_number ? bank_number : artist?.bank.number,
        artist?.bank?.id,
      );
    }
    setShowBankModal(false);
    fetchArtist();
  };

  const handleAddArt = async (e: React.FormEvent) => {
    e.preventDefault();

    if (!file) return alert('Please upload an artwork');
    else if (!name) return alert('Please fill in artwork name');
    else if (!weight) return alert('please fill in artwork weight');
    else if (width <= 0 || height <= 0 || length <= 0)
      return alert('Please fill in the size correctly');
    else if (!artistName) return alert('Please fill in artist name');
    else if (!/^[a-zA-Z\s]+$/.test(artistName))
      return alert(
        'Artist name must contain only alphabetical'
      );
    // upload file first
    setLoading(true);
    const id = await uploadFile(file.fileObj);
    await addProduct({
      name,
      description,
      price,
      owner: user.id,
      image: id,
      weight,
      height,
      width,
      length,
      artistName,
      technique,
    });
    // done
    setLoading(false);
    router.push('/artist/me');
  };

  useEffect(() => {
    fetchTechniques();
  }, []);

  const fetchTechniques = async () => {
    const resp = await getTechniques();
    console.log(`resp`, resp);
    setTechniques(resp);
  };

  const handleFileChange = (e) => {
    setFile({
      fileObj: e.target.files[0],
      url: URL.createObjectURL(e.target.files[0]),
    });
  };

  return (
    <>
      <Head>
        <title>Upload Asset</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />
      <main className="px-4 py-4 grid-cols-2 gap-8 space-y-8 lg:space-y-0 sm:px-16 sm:py-8 lg:grid 2xl:container 2xl:mx-auto">
        <form
          onSubmit={handleAddArt}
          className="col-span-2 grid-cols-2 gap-8 space-y-8 md:space-y-0 md:grid"
        >
          <div>
            <div className="flex items-center space-x-2">
              <img src="/auction.png" alt="Auction" width="25px" />
              <h4 className="uppercase font-bold">Upload Your Asset</h4>
            </div>
            <div className="mt-2">
              {file ? (
                <img src={file.url} />
              ) : (
                <>
                  <label htmlFor="upload" className="w-full cursor-pointer">
                    <div className="bg-gray-100 w-full h-72 relative flex items-center justify-center">
                      <MdClose className="absolute top-0 right-0 h-6 w-6 m-1" />
                      <img src="/upload.png" alt="Upload" width="40px" />
                    </div>
                  </label>
                  <input
                    id="upload"
                    type="file"
                    onChange={handleFileChange}
                    className="hidden"
                  />
                </>
              )}
            </div>
            <div className="mt-2 flex flex-col justify-between">
              <div>
                <input
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  className="text-2xl w-full pt-2 text-center bg-gray-100 focus:outline-none"
                  placeholder="Name of Artwork"
                />

                <textarea
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                  className="bg-gray-100 text-center w-full mt-2 h-full focus:outline-none"
                  placeholder="Description (240 characters)"
                />
              </div>
            </div>
          </div>
          <div>
            <div className="flex items-center space-x-2">
              <GoGear className="w-6 h-6" />
              <h4 className="text-lg font-bold">Options</h4>
            </div>
            <div className="flex justify-between mt-2">
              <span className="text-lg font-bold">Weight</span>
              <span>
                <input
                  type="number"
                  value={weight}
                  onChange={(e) => setWeight(parseFloat(e.target.value))}
                  className="border border-black font-bold focus:outline-none"
                />
                <span className="text-lg font-bold ml-2">kg</span>
              </span>
            </div>
            <div className="flex justify-between mt-2">
              <span className="text-lg font-bold">Size</span>
              <span className="flex">
                <span className="flex justify-between">
                  <span className="text-lg font-bold ml-2">L</span>
                  <input
                    type="number"
                    value={length}
                    onChange={(e) => setLength(parseFloat(e.target.value))}
                    className="border ml-2 w-8 border-black font-bold focus:outline-none"
                  />
                </span>

                <span className="flex justify-between">
                  <span className="text-lg font-bold ml-2">W</span>
                  <input
                    type="number"
                    value={width}
                    onChange={(e) => setWidth(parseFloat(e.target.value))}
                    className="border ml-2 w-8 border-black font-bold focus:outline-none"
                  />
                </span>

                <span className="flex justify-between">
                  <span className="text-lg font-bold ml-2">H</span>
                  <input
                    type="number"
                    value={height}
                    onChange={(e) => setHeight(parseFloat(e.target.value))}
                    className="border ml-2 w-8 border-black font-bold focus:outline-none"
                  />
                </span>
                <span className="text-lg font-bold ml-2">cm</span>
              </span>
            </div>
            <div className="flex justify-between mt-2">
              <span className="text-lg font-bold">Technique</span>
              <select
                value={technique}
                onChange={(e) => setTechnique(parseInt(e.target.value))}
                className="border border-black"
              >
                {techniques.map((technique) => (
                  <option key={technique.id} value={technique.id}>
                    {technique.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="flex justify-between mt-2">
              <span className="text-lg font-bold">Artist Name</span>
              <input
                type="text"
                value={artistName}
                onChange={(e) => setArtistName(e.target.value)}
                className="border border-black font-bold focus:outline-none"
              />
            </div>
            <div className="flex justify-between mt-2">
              <span className="text-lg font-bold">Bid Ends</span>
              <span className="flex flex-col lg:flex-row">
                <DatePicker
                  className="border border-black font-bold focus:outline-none lg:mr-2"
                  selected={bidEnd}
                  dateFormat="yyyy/MM/dd"
                  onChange={(date) => setBidEnd(date)}
                />
                <DatePicker
                  className="border border-black font-bold focus:outline-none w-24 mt-2 lg:mt-0"
                  selected={bidEndTime}
                  onChange={(date) => setBidEndTime(date)}
                  showTimeSelect
                  showTimeSelectOnly
                  timeIntervals={60}
                  timeCaption="Time"
                  dateFormat="h:mm aa"
                />
              </span>
            </div>
            <div className="mt-2 flex items-center justify-between">
              <div className="flex space-x-2 font-bold">
                <FaTruck className="w-6 h-6" />
                <span>Add Shipping Address</span>
              </div>
              <span
                className="cursor-pointer font-bold"
                onClick={handleClickAddAdress}
              >
                {artist?.shipping_address ? 'Edit' : '+'}
              </span>
            </div>
            <div className={addr_box}>
              <div className="opacity-80">
                <h5>
                  {artist?.shipping_address?.address_number}{' '}
                  {artist?.shipping_address?.office_name}{' '}
                  {artist?.shipping_address?.road}{' '}
                  {artist?.shipping_address?.sub_district}{' '}
                  {artist?.shipping_address?.district}{' '}
                  {artist?.shipping_address?.province}{' '}
                  {artist?.shipping_address?.zip_code}
                </h5>
                <h5>{artist?.shipping_address?.country}</h5>
              </div>
            </div>
            <div className="mt-2 flex items-center justify-between">
              <div className="flex space-x-2 font-bold">
                <FaTruck className="w-6 h-6" />
                <span>Add Bank Account</span>
              </div>
              <span
                className="cursor-pointer font-bold"
                onClick={handleClickAddBank}
              >
                {artist?.bank ? 'Edit' : '+'}
              </span>
            </div>
            <div className={addr_box}>
              <div className="opacity-80">
                <h5>{artist?.bank?.name}</h5>
                <h5>{artist?.bank?.number}</h5>
              </div>
            </div>
            <div className="mt-8 flex items-center justify-between">
              <div className="flex space-x-2 font-bold">
                <AiOutlineGlobal className="w-6 h-6" />
                <span>Country</span>
              </div>
              <span>
                <ReactCountryFlag
                  className="ml-4 mr-2 className=flex items-center"
                  style={{
                    width: '2em',
                    height: '2em',
                  }}
                  countryCode={artist?.shipping_address?.country}
                  svg
                />
                {artist?.shipping_address?.country}
              </span>
            </div>

            <div className="flex justify-between mt-6">
              <div className="flex items-center space-x-2">
                <img src="/auction.png" alt="Auction" width="25px" />
                <span className="text-lg font-bold">Starting Price (USD)</span>
              </div>
              <input
                type="number"
                value={price}
                onChange={(e) => setPrice(parseFloat(e.target.value))}
                className="w-1/4 bg-gray-100 font-bold focus:outline-none"
              />
            </div>

            <div className="flex justify-end mt-3 space-x-2">
              <button className={button}>Save</button>
              <button className={button} type="submit">
                {loading ? 'Loading' : 'Publish'}
              </button>
            </div>
          </div>
        </form>
      </main>

      {showModal && (
        <Modal
          title="Shipping Address"
          confirmText="Confirm"
          setShowModal={setShowModal}
          onConfirm={onSubmitAddress}
        >
          <form className="flex flex-col relative m-2">
            <div className="flex flex-row items-center mb-2 font-bold">
              <AiOutlineHome className="mr-2" />
              <h5>Address</h5>
            </div>
            <div className={input_div}>
              Address Number{' '}
              <input
                placeholder={artist?.shipping_address?.address_number}
                className={input_style}
                onChange={(e) => setAddress_number(e.target.value)}
              ></input>
            </div>
            <div className={input_div}>
              Office Name{' '}
              <input
                placeholder={artist?.shipping_address?.office_name}
                className={input_style}
                onChange={(e) => setOffice_name(e.target.value)}
              ></input>
            </div>
            <div className={input_div}>
              Road{' '}
              <input
                placeholder={artist?.shipping_address?.road}
                className={input_style}
                onChange={(e) => setRoad(e.target.value)}
              ></input>
            </div>
            <div className={input_div}>
              Sub District{' '}
              <input
                placeholder={artist?.shipping_address?.sub_district}
                className={input_style}
                onChange={(e) => setSub_district(e.target.value)}
              ></input>
            </div>
            <div className={input_div}>
              District{' '}
              <input
                placeholder={artist?.shipping_address?.district}
                className={input_style}
                onChange={(e) => setDistrict(e.target.value)}
              ></input>
            </div>
            <div className={input_div}>
              Province{' '}
              <input
                placeholder={artist?.shipping_address?.province}
                className={input_style}
                onChange={(e) => setProvince(e.target.value)}
              ></input>
            </div>
            <div className={input_div}>
              Zip Code{' '}
              <input
                placeholder={artist?.shipping_address?.zip_code}
                className={input_style}
                onChange={(e) => setZip_code(e.target.value)}
              ></input>
            </div>
            <span className="flex items-center mt-4">
              <ReactFlagsSelect
                placeholder="Change Country"
                selected={country}
                onSelect={(code) => setCountry(code)}
                searchable
              />
            </span>
          </form>
        </Modal>
      )}

      {showBankModal && (
        <Modal
          title="Bank Account Detail"
          confirmText="Confirm"
          setShowModal={setShowBankModal}
          onConfirm={onSubmitBank}
        >
          <form className="flex flex-col relative m-2">
            <div className="flex flex-row items-center mb-2 font-bold">
              <AiOutlineHome className="mr-2" />
              <h5>Bank Detail</h5>
            </div>
            <div className={input_div}>
              Bank Name{' '}
              <input
                placeholder={artist?.bank?.name}
                className={input_style}
                onChange={(e) => setBankName(e.target.value)}
              ></input>
            </div>
            <div className={input_div}>
              Bank Number{' '}
              <input
                placeholder={artist?.bank?.number}
                className={input_style}
                onChange={(e) => setBankNumber(e.target.value)}
              ></input>
            </div>
          </form>
        </Modal>
      )}
    </>
  );
}
