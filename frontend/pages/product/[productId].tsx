import React, { useEffect, useState } from 'react';
import { HeartIcon, ShareIcon } from '@heroicons/react/outline';
import { useRouter } from 'next/router';
import { getProductById } from '../../api/product';
import Modal from '../../components/Modal';
import { addBid, getBids } from '../../api/bid';
import { useAppContext } from '../../providers/AppProvider';
import CountDownText from '../../components/CountDownText';
import Head from 'next/head';
import isBeforeNow from '../../helper/date';
import Profile from '../../components/Profile';
import ReactCountryFlag from 'react-country-flag';
import InBiddingBadge from '../../components/InBiddingBadge';
import SoldBadge from '../../components/SoldBadge';
import Link from 'next/link';
import MoreArtLikeThis from '../../components/MoreArtLikeThis';
import {
  getBillingAddressById,
  getShippingAddressById,
} from '../../api/address';
import { getArtistById } from '../../api/auth';

const active =
  'focus:outline-none md:absolute right-0 mt-4 border border-black text-white bg-black rounded-lg block transition ease-out duration-300 hover:bg-white hover:text-black px-8 py-1';
const disabled =
  'focus:outline-none md:absolute right-0 mt-4 border border-black text-white bg-black rounded-lg block px-8 py-1 disabled:opacity-50';

export default function Product() {
  const [artist, setArtist] = useState(null);
  const [product, setProduct] = useState(null);
  const [address, setAddress] = useState(product ? product : null);
  const [showModal, setShowModal] = React.useState(false);
  const [bids, setBids] = useState([]);
  const { user, isAuthenticated } = useAppContext();

  //modal state
  const [bidAmount, setBidAmount] = useState(0);
  const [shipCost, setShipCost] = useState(0);
  const [serviceFee, setService] = useState(0);
  const [totalCost, setTotal] = useState(0);
  const [cardNumber, setCardNumber] = useState('');
  const [holderName, setHolderName] = useState('');
  const [expirationMonth, setExpirationMonth] = useState('');
  const [expirationYear, setExpirationYear] = useState('');
  const [securityCode, setSecurityCode] = useState('');
  const [error, setError] = useState(null);

  const router = useRouter();

  useEffect(() => {
    fetchProduct();
  }, [router.query]);

  useEffect(() => {
    if (user) fetchArtistBilling();
  }, [user]);

  const fetchArtistBilling = async () => {
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  const bill_addr = artist?.billing_address;

  const fetchProduct = async () => {
    if (Object.keys(router.query).length == 0) return; // query is still loading
    const { productId } = router.query;
    const res = await getProductById(productId);
    setProduct(res);
    if (res.shippingAddressId) {
      const addr = await getShippingAddressById(res.shippingAddressId);
      setAddress(addr);
    }
    setBids(await getBids({ product: productId }));
  };

  const handleClickBid = () => {
    if (!isAuthenticated) return router.push('/auth/login');
    setShowModal(true);
    const ShippingCost = +(getWeightPrice() / 31.4).toFixed(2);
    setShipCost(ShippingCost);
    const ServiceFee = +(
      0.15 *
      (bidAmount + +(getWeightPrice() / 31.47).toFixed(2))
    ).toFixed(2);
    setService(ServiceFee);
    const totalCost = +(bidAmount + +ShippingCost + +ServiceFee).toFixed(2);
    setTotal(totalCost);
  };

  const onSubmitBid = async () => {
    // maybe a little checking
    setError(null);
    const { Omise } = window as any;
    // get payment token

    Omise.setPublicKey('pkey_test_5lm0wcrfudbml6yzghc');
    const card = {
      name: holderName,
      number: cardNumber,
      expiration_month: expirationMonth,
      expiration_year: expirationYear,
      security_code: securityCode,
    };

    function promisifedCreateToken(card) {
      return new Promise((resolve, reject) => {
        Omise.createToken('card', card, (statusCode, response) => {
          if (
            response.object == 'error' ||
            !response.card.security_code_check
          ) {
            resolve({ error: response.message });
          } else {
            resolve(response);
          }
        });
      });
    }

    const omiseResponse: any = await promisifedCreateToken(card);
    if (omiseResponse.error) return setError(omiseResponse.error);

    // add bid
    const tokenId = omiseResponse.id;
    const response = await addBid({
      price: bidAmount,
      bidder: user.id,
      product: product.id,
      tokenId,
    });

    if (response.data.error) {
      return setError(response.data.error);
    }
    // payment success
    // refresshing page
    setShowModal(false);
    fetchProduct();
  };

  const getCurrentPrice = () => {
    const prices = bids.map((bid) => bid.price);
    return Math.max(product?.price, ...prices);
  };

  //HARD CODE
  const zone = () => {
    if (bill_addr?.country == address?.country || bill_addr?.country == 'TH') return 'IN'
    else if (['HK', 'MO', 'MY', 'SC'].includes(bill_addr?.country)) return 'A';
    else if (['AU', 'CN', 'ID', 'NZ', 'KR', 'PH', 'TW', 'VN'].includes(bill_addr?.country)) return 'B';
    else if (['BN', 'KH', 'JP', 'LA', 'MN'].includes(bill_addr?.country)) return 'C';
    else if (
      ['AF', 'BH', 'BD', 'BT', 'EG', 'IN', 'IQ', 'JO', 'KW', 'MV', 'NP',
      'OM','PS', 'QA', 'SA', 'ZA', 'LK', 'SY', 'AE', 'YE'].includes(bill_addr?.country)) return 'D';
    else if (['CA', 'MX', 'US', ].includes(bill_addr?.country)) return 'E';
    else if (['BE', 'FR', 'DE', 'IT', 'NL', 'ES', 'GB'].includes(bill_addr?.country)) return 'F';
    else if (
      ['AL', 'AD', 'AM', 'AT', 'AZ', 'BY', 'BA', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FK', 'FI',
      'GE', 'GI', 'GR', 'GL', 'HU', 'IS', 'IE', 'IL', 'KZ', 'KG', 'LV', 'LI', 'LT', 'LU', 'MK',
      'MT', 'MD', 'MC', 'ME', 'NO', 'PL', 'PT', 'RO', 'RU', 'RS', 'SK', 'SI', 'SE', 'CH', 'TR', 'UA', 'UZ'
    ].includes(bill_addr?.country)) return 'G';
    else return 'H';
  };

  //HARD CODE
  const getWeightPrice = () => {
    if (zone() == 'IN') return 314;
    else if (zone() == 'A') return 990.6;
    else if (zone() == 'B') return 1011.6;
    else if (zone() == 'C') return 1017;
    else if (zone() == 'D') return 989.4;
    else if (zone() == 'E') return 1087.2;
    else if (zone() == 'F') return 1311.6;
    else if (zone() == 'G') return 1575.6;
    else if (zone() == 'H') return 1855.2;
  }
  
  return (
    <div>
      <Head>
        <title>
          {product?.name
            ? `${product?.name} By ${product?.artist_name}`
            : 'Your Asset'}
        </title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />
      <main className="flex justify-center">
        <div className="xl:w-3/4">
          <div className="flex flex-col md:flex-row">
            <div className="md:w-1/2 lg:w-1/3 flex p-4">
              {/* Image here */}
              <div className="relative">
                {product?.bid_end &&
                  (isBeforeNow(product.bid_end) ? (
                    <SoldBadge />
                  ) : (
                    <InBiddingBadge />
                  ))}
                <img src={product?.imageUrl} className="object-cover" />
              </div>
            </div>

            <div className="md:w-1/2 lg:w-2/3 p-4 flex flex-col justify-between">
              <div className="flex flex-col">
                <div className="lg:flex justify-between">
                  <div className="flex justify-between">
                    <h4 className="text-sm italic">
                      Asset Owner:{' '}
                      <Link href={`/artist/${product?.artistId}`}>
                        <a className="hover:opacity-70">
                          FYNSPACE ID: {product?.artistName}
                        </a>
                      </Link>
                    </h4>
                  </div>
                  <p
                    className={
                      product?.bid_end && isBeforeNow(product.bid_end)
                        ? 'lg:text-right mt-2 lg:mt-0'
                        : 'disable'
                    }
                  >
                    {product?.bid_end &&
                      (isBeforeNow(product.bid_end) ? (
                        ''
                      ) : (
                        <CountDownText end={product.bid_end} />
                      ))}
                  </p>
                </div>

                <div className="py-4 flex flex-col lg:flex-row lg:justify-between">
                  <div className="lg:w-2/3">
                    <h1 className="text-3xl font-bold mb-4">{product?.name}</h1>
                    <p className="text-gray-500 lg:w-4/5">
                      {product?.description
                        ? product?.description
                        : 'No Description'}
                    </p>
                    <p className="italic mt-4 font-semibold">
                      Artist Name: {product?.artist_name}
                    </p>
                  </div>
                  <div className="sm:w-2/3 lg:w-1/3 mt-2 lg:mt-0 text-sm font-bold">
                    <div className="lg:mt-12 flex justify-between">
                      WEIGHT:<h4>{product?.product_weight}</h4>
                    </div>
                    <div className="flex justify-between">
                      SIZE:
                      <h4>
                        {product?.product_width} x {product?.product_length} x{' '}
                        {product?.product_height}
                      </h4>
                    </div>
                    <div className="flex justify-between">
                      TECHNIQUE:<h4>{product?.technique?.name}</h4>
                    </div>
                  </div>
                </div>
              </div>

              <div>
                <div>
                  <div className="flex justify-between py-2">
                    <p className="w-1/2">
                      Location: {address?.address_number} {address?.office_name}{' '}
                      {address?.road} {address?.sub_district}{' '}
                      {address?.district} {address?.province}{' '}
                      {address?.zip_code}
                    </p>
                    <p className="ml-8 flex w-1/2 flex items-center">
                      Country:
                      <ReactCountryFlag
                        className="ml-2 mr-2 border border-gray-200"
                        countryCode={address?.country}
                        svg
                      />{' '}
                      {address?.country}
                    </p>
                  </div>
                  <p className="py-2">
                    <HeartIcon
                      className={
                        product?.like > 0
                          ? 'h-5 w-5 inline text-red-600'
                          : 'h-5 w-5 inline'
                      }
                    />{' '}
                    {product?.like ? product?.like : 0}
                    <ShareIcon
                      className={
                        product?.share > 0
                          ? 'ml-2 h-5 w-5 inline text-blue-600'
                          : 'ml-2 h-5 w-5 inline'
                      }
                    />{' '}
                    {product?.share ? product?.share : 0}
                  </p>
                </div>

                <div>
                  <div className="flex justify-between text-xl mt-4">
                    <span className="font-bold w-1/2 flex flex-col lg:flex-row justify-between mr-4">
                      <h1>Current Price</h1>
                      <h1>{getCurrentPrice()} $</h1>
                    </span>
                    <span className="flex flex flex-col lg:flex-row font-bold justify-between ml-4 w-1/2 border px-2 py-1 rounded-xl border-gray-600">
                      <h1>Marked Price</h1>
                      <h1 className="text-red-600">{product?.price} $</h1>
                    </span>
                  </div>

                  <div className="relative">
                    <span className="mt-4 flex flex flex-col lg:flex-row lg:justify-between text-2xl font-bold lg:items-center">
                      <span className="flex items-center">
                        <img src="/auction.png" alt="Auction" width="25px" />
                        <h1 className="px-2">Place a Bid</h1>
                      </span>
                      <form className="mt-2 lg:mt-0 lg:w-1/2 text-base font-normal flex justify-between">
                        <input
                          placeholder="Minimum 10$ from current price"
                          className="w-full p-2 bg-gray-100 rounded-lg focus:outline-none"
                          onChange={(e) =>
                            setBidAmount(parseInt(e.target.value))
                          }
                        />
                        <span className="text-xl font-bold pl-2">USD</span>
                      </form>
                    </span>
                    <button
                      onClick={handleClickBid}
                      className={
                        bidAmount >= getCurrentPrice() + 10 ? active : disabled
                      }
                      disabled={
                        bidAmount < getCurrentPrice() + 10 || !bidAmount
                      }
                    >
                      Bid
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      <footer className="py-12 flex justify-center">
        <div className="xl:w-3/4">
          <MoreArtLikeThis />
        </div>
      </footer>

      {showModal && (
        <Modal
          title="Your Order"
          confirmText="Place a bid"
          setShowModal={setShowModal}
          onConfirm={onSubmitBid}
        >
          <Head>
            <script src="https://cdn.omise.co/omise.js"></script>
          </Head>
          <div className="relative p-6 flex-auto">
            <div>
              <p className="flex justify-between">
                Current Bid: <span>{getCurrentPrice()} $</span>
              </p>
              <label className="flex justify-between">
                Your Bidding Amount <span>{bidAmount} $</span>
              </label>
              <p className="flex justify-between">
                Shipping Cost: <span>{shipCost} $</span>
              </p>
              <p className="text-sm pb-1 text-gray-500">{zone() == 'IN'? `Shipping To ${bill_addr?.country} (Domestic)` : `Shipping To ${bill_addr?.country}`}</p>
              <p className="flex justify-between">
                Service Fee 15%: <span>{serviceFee} $</span>
              </p>
              <p className="mt-2 flex justify-between text-lg font-bold">
                Total: <span>{totalCost} $</span>
              </p>
            </div>
            <p className="font-bold pt-4">Payment info</p>
            <div className="">
              <div className="flex justify-between my-2">
                Credit Card no.
                <input
                  placeholder="Credit Card no."
                  className="border border-black"
                  type="text"
                  value={cardNumber}
                  onChange={(e) => setCardNumber(e.target.value)}
                />
              </div>
              <div className="flex justify-between my-2">
                Holder Name
                <input
                  placeholder="Holder name"
                  className="border border-black"
                  type="text"
                  value={holderName}
                  onChange={(e) => setHolderName(e.target.value)}
                />
              </div>
              <span className="flex justify-between my-2">
                Expiry Date
                <span>
                  <input
                    className="w-12 border border-black"
                    placeholder="MM"
                    type="text"
                    value={expirationMonth}
                    onChange={(e) => setExpirationMonth(e.target.value)}
                  />{' '}
                  /{' '}
                  <input
                    className="w-12 border border-black"
                    placeholder="YY"
                    type="text"
                    value={expirationYear}
                    onChange={(e) => setExpirationYear(e.target.value)}
                  />
                </span>
              </span>
              <span className="flex justify-between my-2">
                Security Code
                <input
                  className="w-28 border border-black"
                  placeholder="Security code"
                  type="text"
                  value={securityCode}
                  onChange={(e) => setSecurityCode(e.target.value)}
                />
              </span>
              {error && <h1 className="text-red-500">{error}</h1>}
            </div>
            <h3 className="mt-4 mb-2 font-bold">Bid History</h3>
            {bids
              ?.sort((a, b) => b.price - a.price)
              .map(({ id, price, bidder_name, created_at }) => (
                <div key={id} className="flex justify-between items-center">
                  <span className="font-semibold">{bidder_name}</span>
                  <span className="text-sm">{created_at}</span>
                  <span className="font-semibold">{price}</span>
                </div>
              ))}
          </div>
        </Modal>
      )}
    </div>
  );
}
