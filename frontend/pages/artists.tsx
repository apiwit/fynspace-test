import Head from 'next/head';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import { getArtists } from '../api/auth';

export default function artists() {
  const [artists, setArtists] = useState([]);

  const fetchArtists = async () => {
    const artists = await getArtists();
    setArtists(artists);
  };

  useEffect(() => {
    fetchArtists();
  }, []);

  return (
    <div>
      <Head>
        <title>Artists</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <main className="2xl:container 2xl:mx-auto">
        <div className="mx-8 mt-2">
          <a href="#" className="uppercase text-2xl">
            Artists
          </a>
        </div>
        <div className="px-8 mt-2 grid gap-3 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
          {artists.map(({ id, username, created_at, products, image }) => (
            <Link key={id} href={`/artist/${id}`}>
              <a className="h-96 relative cursor-pointer">
                <div className="border border-gray-200 bg-white rounded-xl shadow-md h-96 relative">
                  <img
                    src={image}
                    alt={username}
                    className=" mt-3 w-full h-3/4 object-cover"
                  />
                  <div className="p-1">
                    <h5 className="font-bold">{username}</h5>
                    <p className="font-semibold">Joined: {created_at}</p>
                    <p className="font-semibold">
                      Asset(s) Posession: {products.length}
                    </p>
                  </div>
                </div>
              </a>
            </Link>
          ))}
        </div>
      </main>
    </div>
  );
}
