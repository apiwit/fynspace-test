import '../styles/globals.css';
import 'tailwindcss/tailwind.css';
import React from 'react';
import Layout from '../components/Layout';
import { AppProvider } from '../providers/AppProvider';

function MyApp({ Component, pageProps }) {
  return (
    <AppProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </AppProvider>
  );
}

export default MyApp;
