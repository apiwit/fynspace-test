import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { getArtistById } from '../api/auth';
import { useAppContext } from '../providers/AppProvider';
import { FiHeart, FiPlus } from 'react-icons/fi';
import { GiCutDiamond } from 'react-icons/gi';
import Profile from '../components/Profile';
import Head from 'next/head';
import Wishlist from '../components/Wishlist';
import { BiTimer } from 'react-icons/bi';
import isBeforeNow from '../helper/date';
import { RiVipCrown2Line } from 'react-icons/ri';
import SoldBadge from '../components/SoldBadge';
import InBiddingBadge from '../components/InBiddingBadge';

export default function me() {
  const [artist, setArtist] = useState(null);
  const { user } = useAppContext();

  const router = useRouter();
  useEffect(() => {
    if (user) fetchArtist();
  }, [user]);

  const fetchArtist = async () => {
    console.log(`user.id`, user.id);
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  return (
    <div>
      <Head>
        <title>Wishlist</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />
      <div>
        <div className="px-4 flex items-center space-x-2 sm:px-8">
          <GiCutDiamond className="w-6 h-6" />
          <h1 className="uppercase font-bold">Wishlist</h1>
        </div>

        <div className="px-8 mt-2 grid gap-3 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
          {artist?.products.map(
            ({
              id,
              name,
              imageUrl,
              bid_end,
              like,
              artist_name,
              product_weight,
              product_height,
              product_length,
              product_width,
              technique,
            }) => (
              <Link href={`/product/${id}`} key={id}>
                <a className="relative">
                  <div className="bg-white rounded-xl shadow-md h-auto relative">
                    <BiTimer className="w-8 h-8 ml-2 my-2" />
                    <img
                      src={imageUrl}
                      alt={name}
                      className="w-full h-56 object-cover"
                    />

                    <div className="px-2 py-1">
                      <div className="flex justify-between">
                        <span className="flex flex-col">
                          <span className="flex items-center">
                            <h5 className="flex text-sm items-center font-bold">
                              <RiVipCrown2Line className="mr-1" />
                              Won By:
                            </h5>
                            <h5 className="text-xs ml-2">01A45H</h5>
                          </span>
                          <span className="mt-2 text-xs italic">
                            Art By ID: {artist?.username}
                          </span>
                        </span>
                        <span className="font-bold text-right">
                          {bid_end && isBeforeNow(bid_end)
                            ? 'Final Price'
                            : 'Current Price'}
                          <p className="text-green-400">$ 12345</p>
                        </span>
                      </div>

                      <div className="mt-4 flex justify-between font-bold">
                        <div>
                          <p className="text-xl">{name}</p>
                          <p className="text-xs font-normal italic">
                            Artist Name: {artist_name ? artist_name : 'Unknown'}
                          </p>
                        </div>
                        <div className="text-xs w-1/2 sm:w-2/5 lg:w-1/2">
                          <div className="flex justify-between">
                            WEIGHT:
                            <h4>{product_weight ? product_weight : 0}</h4>
                          </div>
                          <div className="flex justify-between">
                            SIZE:
                            <h4>
                              {product_width ? product_width : 0} x{' '}
                              {product_length ? product_length : 0} x{' '}
                              {product_height ? product_height : 0}
                            </h4>
                          </div>
                          <div className="flex justify-between">
                            TECHNIQUE:
                            <h4 className="text-right">
                              {technique?.name
                                ? technique?.name
                                : 'Unknown Painting'}
                            </h4>
                          </div>
                        </div>
                      </div>

                      <div className="mt-4 relative">
                        {/*Bidding/Sold Badge*/}
                      </div>

                      <div className="mt-2 flex justify-between">
                        <span className="flex items-center">
                          <FiHeart className="text-red-500 mr-2" />
                          {like}
                        </span>
                        <span className="text-sm text-right mt-2">
                          Shipping Status:{' '}
                          {bid_end && isBeforeNow(bid_end)
                            ? 'Bid End'
                            : 'In Bidding'}
                        </span>
                      </div>
                    </div>
                  </div>
                </a>
              </Link>
            )
          )}
        </div>
      </div>
    </div>
  );
}
