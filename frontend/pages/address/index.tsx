import 'tailwindcss/tailwind.css';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { useAppContext } from '../../providers/AppProvider';
import { AiOutlineHome } from 'react-icons/ai';
import Head from 'next/head';
import ProfileMenu from '../../components/ProfileMenu';
import Profile from '../../components/Profile';
import Wishlist from '../../components/Wishlist';
import MyAssest from '../../components/MyAssest';
import ReactCountryFlag from 'react-country-flag';
import { getArtistById } from '../../api/auth';
import { FaTruck } from 'react-icons/fa';

const edit_button =
  'border border-black bg-white text-black rounded-lg block transition ease-out duration-300 hover:bg-black hover:text-white absolute top-0 right-0 px-2 m-2 focus:outline-none';
const addr_box =
  'relative rounded-xl bg-white shadow-lg border border-gray-200 p-2 mt-2 min-w-full focus:outline-none';

export default function Address() {
  const [artist, setArtist] = useState(null);
  const { user } = useAppContext();

  const router = useRouter();
  useEffect(() => {
    if (user) fetchArtist();
  }, [user]);

  const fetchArtist = async () => {
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  const bill = artist?.billing_address;
  const ship = artist?.shipping_address;

  return (
    <div>
      <Head>
        <title>Billing Address</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />

      <div className="flex flex-col xl:flex-row">
        <div className="m-4 flex flex-col xl:w-3/4">
          <div className="flex flex-col xl:flex-row">
            <ProfileMenu />
            <div className="lg:flex flex-col lg:justify-center xl:justify-start xl:w-2/3">
              <div className="ml-8 mr-8 lg:w-3/4">
                <h1 className="font-bold mt-8 xl:mt-2 mb-4">Billing Address</h1>
                <div className={addr_box}>
                  <span className="font-bold mb-2 flex flex-row items-center">
                    <AiOutlineHome className="mr-2" /> Your Address
                  </span>
                  <div className="opacity-80">
                    <h5>
                      {bill?.address_number} {bill?.office_name} {bill?.road}{' '}
                      {bill?.sub_district} {bill?.district} {bill?.province}{' '}
                      {bill?.zip_code} {bill?.mobile}
                    </h5>
                    <h5 className="flex items-center">
                      <ReactCountryFlag
                        className="mr-2 flex items-center"
                        countryCode={bill?.country}
                        svg
                      />{' '}
                      {bill?.country}
                    </h5>
                  </div>
                  <div className="opacity-60 mt-4">
                    <h5>Last Update At: {bill?.updated_at}</h5>
                  </div>
                  <Link
                    href={
                      artist?.billing_address
                        ? `address/billing/${bill?.id}`
                        : `address/billing/add`
                    }
                  >
                    <button className={edit_button}>
                      {artist?.billing_address ? 'Edit' : 'Add New'}
                    </button>
                  </Link>
                </div>
              </div>
              <div className="ml-8 mr-8 mt-4 lg:w-3/4">
                <h1 className="font-bold mt-8 xl:mt-2 mb-4">
                  Shipping Address
                </h1>
                <div className={addr_box}>
                  <span className="font-bold mb-2 flex flex-row items-center">
                    <FaTruck className="mr-2" /> Your Address
                  </span>
                  <div className="opacity-80">
                    <h5>
                      {ship?.address_number} {ship?.office_name} {ship?.road}{' '}
                      {ship?.sub_district} {ship?.district} {ship?.province}{' '}
                      {ship?.zip_code}
                    </h5>
                    <h5 className="flex items-center">
                      <ReactCountryFlag
                        className="mr-2 className=flex items-center"
                        countryCode={ship?.country}
                        svg
                      />{' '}
                      {ship?.country}
                    </h5>
                  </div>
                  <div className="opacity-60 mt-4">
                    <h5>Last Update At: {ship?.updated_at}</h5>
                  </div>
                  <Link
                    href={
                      ship
                        ? `address/shipping/${ship?.id}`
                        : `address/shipping/add`
                    }
                  >
                    <button className={edit_button}>
                      {ship ? 'Edit' : 'Add New'}
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <MyAssest />
        </div>
        <Wishlist />
      </div>
    </div>
  );
}
