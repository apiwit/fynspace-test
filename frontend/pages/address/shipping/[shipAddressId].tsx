import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { ArrowLeftIcon } from '@heroicons/react/outline';
import ProfileMenu from '../../../components/ProfileMenu';
import { AiOutlineHome } from 'react-icons/ai';
import Profile from '../../../components/Profile';
import { useRouter } from 'next/router';
import {
  getShippingAddressById,
  UpdateShippingAddress,
} from '../../../api/address';
import ReactCountryFlag from 'react-country-flag';
import ReactFlagsSelect from 'react-flags-select';

const info =
  'bg-white rounded-lg shadow-md mt-2 mb-2 p-2 border border-black border-opacity-20';
const input_div =
  'flex justify-between flex-col sm:flex-row';
const input_style =
  'my-1 p-1 border-2 rounded-lg border-black focus:outline-none max-h-32 sm:w-1/2';
const submit_button =
  'border border-black bg-white rounded-lg block transition ease-out duration-300 hover:bg-black hover:text-white text-black px-2 py-1 mt-4 mb-2 focus:outline-none';

export default function EditShipAddress() {
  const [address, setAddress] = useState(null);
  const [address_number, setAddress_number] = useState(address?.address_number);
  const [office_name, setOffice_name] = useState(address?.office_name);
  const [road, setRoad] = useState(address?.road);
  const [sub_district, setSub_district] = useState(address?.sub_district);
  const [district, setDistrict] = useState(address?.district);
  const [province, setProvince] = useState(address?.province);
  const [zip_code, setZip_code] = useState(address?.zip_code);
  const [country, setCountry] = useState(address?.country);
  const [loading, setLoading] = useState(false);

  const router = useRouter();
  const { shipAddressId } = router.query;

  useEffect(() => {
    fetchAddress();
  }, [router.query]);

  const fetchAddress = async () => {
    if (Object.keys(router.query).length == 0) return; // query is still loading
    const { shipAddressId } = router.query;
    const address = await getShippingAddressById(shipAddressId);
    setAddress(address);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    await UpdateShippingAddress(
      address_number? address_number : address?.address?.address_number,
      office_name? office_name : address?.office_name,
      road? road : address?.road,
      sub_district? sub_district : address?.sub_district,
      district? district : address?.district,
      province? province : address?.province,
      zip_code? zip_code : address?.zip_code,
      country? country : address?.country,
      shipAddressId
    );
    setLoading(false);
    e.target.reset();
    fetchAddress();
  };

  return (
    <div>
      <Head>
        <title>Update Your Shipping Address</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <Profile />
      <div className="flex flex-col xl:flex-row">
        <ProfileMenu />
        <div className="md:flex md:justify-center xl:justify-start xl:w-4/5 ml-2 mr-2 mt-8">
          <div className="flex flex-col md:w-3/5">
            <span
              onClick={() => router.back()}
              className="cursor-pointer mr-4 flex font-semibold"
            >
              <ArrowLeftIcon className="h-5 w-5 mr-1" />
              Back
            </span>
            <div className={info}>
              <form onSubmit={handleSubmit} className="flex flex-col relative">
                <div className="flex flex-row items-center mb-2 font-bold">
                  <AiOutlineHome className="mr-2" />
                  <h5>Address</h5>
                </div>
                <div className={input_div}>
                  Address Number{' '}
                  <input
                    placeholder={address?.address_number}
                    className={input_style}
                    onChange={(e) => setAddress_number(e.target.value)}
                  ></input>
                </div>
                <div className={input_div}>
                  Office Name{' '}
                  <input
                    placeholder={address?.office_name}
                    className={input_style}
                    onChange={(e) => setOffice_name(e.target.value)}
                  ></input>
                </div>
                <div className={input_div}>
                  Road{' '}
                  <input
                    placeholder={address?.road}
                    className={input_style}
                    onChange={(e) => setRoad(e.target.value)}
                  ></input>
                </div>
                <div className={input_div}>
                  Sub District{' '}
                  <input
                    placeholder={address?.sub_district}
                    className={input_style}
                    onChange={(e) => setSub_district(e.target.value)}
                  ></input>
                </div>
                <div className={input_div}>
                  District{' '}
                  <input
                    placeholder={address?.district}
                    className={input_style}
                    onChange={(e) => setDistrict(e.target.value)}
                  ></input>
                </div>
                <div className={input_div}>
                  Province{' '}
                  <input
                    placeholder={address?.province}
                    className={input_style}
                    onChange={(e) => setProvince(e.target.value)}
                  ></input>
                </div>
                <div className={input_div}>
                  Zip Code{' '}
                  <input
                    placeholder={address?.zip_code}
                    className={input_style}
                    onChange={(e) => setZip_code(e.target.value)}
                  ></input>
                </div>
                <p className="flex flex-row items-center my-2 font-bold">
                  <AiOutlineHome className="mr-2" />
                  Country
                </p>
                <h5 className="flex items-center">
                  Current:
                  <ReactCountryFlag
                    className="ml-2 mr-2 flex items-center"
                    countryCode={address?.country}
                    svg
                  />
                  {address?.country}
                </h5>

                <span className="mt-4 flex ">
                  <ReactFlagsSelect
                    className="focus:outline-none w-full"
                    placeholder="Select Country"
                    selected={country}
                    onSelect={(code) => setCountry(code)}
                    searchable
                  />
                </span>
                <button className={submit_button} type="submit">
                  {loading ? 'Loading' : 'Update'}
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
