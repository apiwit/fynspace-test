import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import ReactMarkdown from 'react-markdown'
import { getToS } from '../api/about'
import HelpFooter from '../components/HelpFooter';

export default function ToS() {
  const [about, setAbout] = useState(null);
  
  const fetchToS = async () => {
    const res = await getToS();
    setAbout(res);
  };

  useEffect(() => {
    fetchToS();
  }, []);

  //In strapi rich text: \n + 2 space for line break
  const term = about?.tos.replace(/\n/gi, '\n &nbsp;');
  const appendix = about?.appendix_a.replace(/\n/gi, '\n &nbsp;');

  return (
    <div>
      <Head>
        <title>Terms of Service</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <div className="h-32 bg-black"></div>
      <div className="flex justify-center">
        <div className="flex flex-col mt-16 mb-8 w-4/5 xl:w-1/2">
          <h1 className="mb-4 uppercase text-2xl font-bold border-b-2 border-black">
            Terms of Service
          </h1>
          <ReactMarkdown
            children={term}
            className="reactMarkDown"
            />
          <h1 className="mt-8 mb-8 py-4 uppercase text-2xl font-bold border-t-2 border-b-2 border-black">
            <p>APPENDIX A –</p>
            <p>SERVICE AND PAYMENT AGREEMENT</p>
          </h1>
          <ReactMarkdown
            children={appendix}
            className="reactMarkDown"
            />
          <HelpFooter />
        </div>
      </div>
    </div>
  );
}
