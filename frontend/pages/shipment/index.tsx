import 'tailwindcss/tailwind.css';
import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import ProfileMenu from '../../components/ProfileMenu';
import Profile from '../../components/Profile'
import Wishlist from '../../components/Wishlist';
import MyAssest from '../../components/MyAssest';
import { useAppContext } from '../../providers/AppProvider';
import { useRouter } from 'next/router';
import { getArtistById } from '../../api/auth';

const input_style =
  'mt-2 mb-2 p-2 border-2 rounded-lg border-black focus:outline-none';

export default function Shipment() {
  const [artist, setArtist] = useState(null);
  const { user } = useAppContext();

  const router = useRouter();
  useEffect(() => {
    if (user) fetchArtist();
  }, [user]);

  const fetchArtist = async () => {
    console.log(`user.id`, user.id);
    const result = await getArtistById(user.id);
    setArtist(result);
  };

  return (
    <div>
      <Head>
        <title>Shipping Update</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>
      <Profile />

      <div className="flex flex-col xl:flex-row">
        <div className="m-4 flex flex-col xl:w-3/4">
          <div className="flex flex-col xl:flex-row">
            <ProfileMenu />
            <div className="ml-8 mr-8 flex flex-col xl:w-3/4">
              <h1 className="font-bold mt-8 mb-4 xl:mt-0">My Shipment</h1>
              <input
                className={input_style}
                placeholder="Search From Track Number"
              ></input>
              {artist?.shipping_products.map(
                ({ id, trackno, name, status, imageUrl, location }) => (
                  <Link href={`/shipment/${id}`} key={id}>
                    <a className="relative">
                      <div className="flex flex row rounded-xl bg-white shadow-lg border border-gray-200 m-2">
                        <div className="w-2/5 m-4">
                          <img src={imageUrl} alt={name} className="w-full" />
                        </div>
                        <div className="m-4">
                          <h5>Track No. {trackno}</h5>
                          <h5>{name}</h5>
                          <h5>{status}</h5>
                          <h5>Ship to {location}</h5>
                        </div>
                      </div>
                    </a>
                  </Link>
                )
              )}
            </div>
          </div>
          <MyAssest />
        </div>
        <Wishlist />
      </div>
    </div>
  );
}
