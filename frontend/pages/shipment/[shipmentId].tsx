import React, { useEffect, useState } from 'react';
import Head from 'next/head';
import { ArrowLeftIcon } from '@heroicons/react/outline';
import ProfileMenu from '../../components/ProfileMenu';
import Wishlist from '../../components/Wishlist';
import MyAssest from '../../components/MyAssest';
import Profile from '../../components/Profile';
import axios from 'axios';
import { useRouter } from 'next/router';
import { getShipmentById } from '../../api/shipping';

const info =
  'bg-white rounded-lg shadow-md mt-2 mb-2 p-2 border border-black border-opacity-20';
const input_style =
  'mt-2 mb-2 p-1 border-2 rounded-lg border-black focus:outline-none';
const submit_button =
  'border border-black bg-white rounded-lg block transition ease-out duration-300 hover:bg-black hover:text-white text-black px-2 py-1 ml-2 focus:outline-none';
const green =
  'bg-green-500 rounded-lg block transition ease-out duration-300 hover:bg-green-600 text-white px-2 py-1 mt-2 focus:outline-none';
const blue =
  'bg-blue-500 rounded-lg block transition ease-out duration-300 hover:bg-blue-700 text-white px-2 py-1 mt-2 focus:outline-none';

export default function UpdateShipping() {
  const [product, setProduct] = useState(null);
  const [trackno, setTrackno] = useState(null);
  const [loading, setLoading] = useState(false);

  const router = useRouter();
  const { shipmentId } = router.query;

  useEffect(() => {
    fetchProduct();
  }, [router.query]);

  const fetchProduct = async () => {
    if (Object.keys(router.query).length == 0) return; // query is still loading
    const { shipmentId } = router.query;
    const shipment = await getShipmentById(shipmentId);
    setProduct(shipment);
  };

  const handleClick = async () => {
    await axios.put(`http://localhost:1337/shipping-products/${shipmentId}`, {
      isShipped: product?.isShipped ? false : true,
    });
    fetchProduct();
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    if (trackno) {
      await axios.put(`http://localhost:1337/shipping-products/${shipmentId}`, {
        trackno: trackno,
      });
    }
    setLoading(false);
    setTrackno(null);
    e.target.reset();
    fetchProduct();
  };

  return (
    <div>
      <Head>
        <title>Shipping Update - {product?.name}</title>
        <link rel="icon" type="image/png" href="/fynspace-icon.png" />
      </Head>

      <Profile />
      <div className="flex flex-col xl:flex-row">
        <ProfileMenu />

        <div className="lg:flex lg:justify-center xl:justify-start xl:w-3/5 ml-2 mr-2 mt-8">
          <div className="md:flex md:flex-row">
            <div className="md:w-3/4">
              <span
                onClick={() => router.back()}
                className="cursor-pointer mr-4 flex font-semibold"
              >
                <ArrowLeftIcon className="h-5 w-5 mr-1" />
                Back
              </span>
              <div className={info}>
                <h5>Assests Name: {product?.name} </h5>
                <h5>Tracking No. {product?.trackno}</h5>
                <form className="flex items-center" onSubmit={handleSubmit}>
                  <input
                    placeholder="Update Track Number"
                    className={input_style}
                    onChange={(e) => setTrackno(e.target.value)}
                  ></input>
                  <button className={submit_button} type="submit">
                    {loading ? 'Loading' : 'Update'}
                  </button>
                </form>
              </div>
              <div className={info}>
                <h5>Transaction Date: {product?.created_at}</h5>
                <h5>Amount {product?.amount} USD</h5>
              </div>
              <div className={info}>
                <h1 className="font-bold">Shipment</h1>
                <h5>
                  Status: {product?.isShipped ? 'Shipped' : 'Not Shipped'}
                </h5>
                <h5>Shipping Type: (Domestic/International)</h5>
                <button
                  onClick={handleClick}
                  className={product?.isShipped ? green : blue}
                >
                  {product?.isShipped ? 'Shipped' : 'Mark as Shipped'}
                </button>
              </div>
            </div>
            <div className="md:w-1/4 ml-2">
              Image Preview
              <img
                src={product?.imageUrl}
                alt={product?.name}
                className="w-full mt-2 mb-2"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
