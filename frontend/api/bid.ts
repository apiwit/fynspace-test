import axios from 'axios';
import Cookie from 'js-cookie';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export const addBid = async ({ price, bidder, product, tokenId }) => {
  const token = Cookie.get('token');
  const response = await axios.post(
    `${API_URL}/bids`,
    {
      bidder,
      price,
      product,
      tokenId,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  return response as any;
};

export const getBids = async ({ product }) => {
  const response = await axios.get(`${API_URL}/bids?product=${product}`);
  return response.data.map(({ bidder, created_at, id, price }) => ({
    bidder_name: bidder.username,
    created_at,
    id,
    price,
  }));
};
