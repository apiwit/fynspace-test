import axios from 'axios';
import Cookie from 'js-cookie';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export const getShippingProducts = async () => {
  const response = await axios.get(`${API_URL}/shipping-products`);
  return response.data.map(
    ({ id, trackno, name, isShipped, image, location, amount, created_at }) => ({
      id,
      trackno,
      name,
      isShipped,
      imageUrl: image[0].url,
      location,
      amount,
      created_at
    })
  );
};

export const getShipmentById = async (productId: any) => {
  const {
    data: { id, trackno, image, name, isShipped, location, amount, created_at },
  } = await axios.get(`${API_URL}/shipping-products/${productId}`);
  return {
    id,
    trackno,
    name,
    imageUrl: image[0].url,
    isShipped,
    location,
    amount,
    created_at
  };
};

export const addShippingProduct = async ({
  id,
  trackno,
  name,
  image,
  isShipped,
  location,
  amount,
  created_at,
}) => {
  const token = Cookie.get('token');
  const response = await axios.post(
    `${API_URL}/shipping-products`,
    {
      id,
      trackno,
      name,
      image,
      isShipped,
      location,
      amount,
      created_at,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  console.log(`response`, response);
};