import axios from 'axios';
import Cookie from 'js-cookie';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export const getBillingAddress = async () => {
  const response = await axios.get(`${API_URL}/billing-addresses`);
  return response.data.map(
    ({
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      mobile,
      updated_at,
      id,
    }) => ({
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      mobile,
      updated_at,
      id,
    })
  );
};

export const getShippingAddress = async () => {
  const response = await axios.get(`${API_URL}/shipping-addresses`);
  return response.data.map(
    ({
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      updated_at,
      id,
    }) => ({
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      updated_at,
      id,
    })
  );
};

export const getBillingAddressById = async (billAddressId: any) => {
  const {
    data: {
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      mobile,
      updated_at,
      id,
    },
  } = await axios.get(`${API_URL}/billing-addresses/${billAddressId}`);
  return {
    address_number,
    office_name,
    road,
    sub_district,
    district,
    province,
    zip_code,
    country,
    mobile,
    updated_at,
    id,
  };
};

export const getShippingAddressById = async (shipAddressId: any) => {
  const {
    data: {
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      updated_at,
      id,
    },
  } = await axios.get(`${API_URL}/shipping-addresses/${shipAddressId}`);
  return {
    address_number,
    office_name,
    road,
    sub_district,
    district,
    province,
    zip_code,
    country,
    updated_at,
    id,
  };
};

export const addBillingAddress = async ({
  address_number,
  office_name,
  road,
  sub_district,
  district,
  province,
  zip_code,
  country,
  mobile,
  owner,
}) => {
  const token = Cookie.get('token');
  const response = await axios.post(
    `${API_URL}/billing-addresses`,
    {
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      mobile,
      owner,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  console.log(`response`, response);
};

export const addShippingAddress = async ({
  address_number,
  office_name,
  road,
  sub_district,
  district,
  province,
  zip_code,
  country,
  owner,
}) => {
  const token = Cookie.get('token');
  const response = await axios.post(
    `${API_URL}/shipping-addresses`,
    {
      address_number,
      office_name,
      road,
      sub_district,
      district,
      province,
      zip_code,
      country,
      owner,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  console.log(`response`, response);
};

export const UpdateShippingAddress = async (
  address_number: any,
  office_name: any,
  road: any,
  sub_district: any,
  district: any,
  province: any,
  zip_code: any,
  country: any,
  shipAddressId: any
) => {
  await axios.put(`${API_URL}/shipping-addresses/${shipAddressId}`, {
    address_number: address_number,
    office_name: office_name,
    road: road,
    sub_district: sub_district,
    district: district,
    province: province,
    zip_code: zip_code,
    country: country,
  });
};

export const UpdateBillingAddress = async (
  address_number: any,
  office_name: any,
  road: any,
  sub_district: any,
  district: any,
  province: any,
  zip_code: any,
  country: any,
  mobile: any,
  billAddressId: any
) => {
  await axios.put(`${API_URL}/billing-addresses/${billAddressId}`, {
    address_number: address_number,
    office_name: office_name,
    road: road,
    sub_district: sub_district,
    district: district,
    province: province,
    zip_code: zip_code,
    country: country,
    mobile: mobile,
  });
};