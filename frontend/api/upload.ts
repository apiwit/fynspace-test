import axios from 'axios';
import Cookie from 'js-cookie';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export const uploadFile = async (fileObj) => {
  const token = Cookie.get('token');

  const formData = new FormData();
  formData.append('files', fileObj);
  const response = await axios.post(`${API_URL}/upload`, formData, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  console.log(`response`, response);
  return response.data[0].id;
};
