import axios from 'axios';
import Cookie from 'js-cookie';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export async function getTechniques() {
  const response = await axios.get(`${API_URL}/techniques`);
  return response.data.map(({ name, id }) => ({
    name,
    id,
  }));
}

export const getProducts = async () => {
  const response = await axios.get(`${API_URL}/products`);
  return response.data.map(
    ({ name, description, id, image, price, bid_end, like }) => ({
      name,
      description,
      id,
      imageUrl: image[0].url,
      price,
      bid_end,
      like,
    })
  );
};

export const getProductById = async (productId: any) => {
  const {
    data: {
      created_at,
      description,
      id,
      image,
      name,
      owner,
      artist_name,
      product_weight,
      product_height,
      product_length,
      product_width,
      technique,
      price,
      bids,
      bid_end,
      address,
      country,
      like,
      share,
    },
  } = await axios.get(`${API_URL}/products/${productId}`);
  return {
    created_at,
    bid_end,
    description,
    id,
    imageUrl: image[0].url,
    name,
    artistName: owner.username,
    artistId: owner.id,
    shippingAddressId: owner.shipping_address,
    artist_name,
    product_weight,
    product_height,
    product_length,
    product_width,
    technique,
    price,
    address,
    country,
    like,
    share,
    bids: bids?.map(({ id, bidder, price, created_at }) => ({
      id,
      bidder,
      price,
      created_at,
    })),
  };
};

export const addProduct = async ({
  name,
  description,
  price,
  owner,
  image,
  weight,
  height,
  width,
  length,
  artistName,
  technique,
}) => {
  const token = Cookie.get('token');
  const response = await axios.post(
    `${API_URL}/products`,
    {
      name,
      description,
      price,
      owner,
      image,
      weight,
      artist_name: artistName,
      product_weight: weight,
      product_height: height,
      product_width: width,
      product_length: length,
      technique,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );

  console.log(`response`, response);
};
