/* /api/auth.js */

import Cookie from 'js-cookie';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';

//const API_URL = process.env.NEXT_PUBLIC_API_URL || 'https://api-user.fynspace.com';
const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

//register a new user
export const registerUser = async (email, password) => {
  //prevent function from being ran on the server
  if (typeof window === 'undefined') return;

  const response = await axios.post(`${API_URL}/auth/local/register`, {
    // username: uuidv4(),
    email,
    password,
  });
  Cookie.set('token', response.data.jwt);
  return response.data.user;
  //resolve the promise to set loading to false in SignUp form
};

export const loginUser = async (identifier, password) => {
  //prevent function from being ran on the server
  if (typeof window === 'undefined') return;

  const response = await axios.post(`${API_URL}/auth/local/`, {
    identifier,
    password,
  });

  Cookie.set('token', response.data.jwt);
  return response.data.user;
};

export const logout = async () => {
  //remove token and user cookie
  Cookie.remove('token');
  // @ts-ignore: no reason
  delete window.__user;

  // sync logout between multiple windows
  window.localStorage.setItem('logout', Date.now().toString());
  //redirect to the home page
};

export const changePassword = async (oldPW, newPW, confirmPW) => {
  //prevent function from being ran on the server
  if (typeof window === 'undefined') return;

  //Unfinished
};

export const getArtists = async () => {
  const response = await axios.get(`${API_URL}/users`);
  return response.data.map(({ id, username, created_at, products, image }) => ({
    image: image[0]?.url,
    username,
    id,
    created_at,
    products,
  }));
  // return response.data;
};

export const getArtistById = async (artistId) => {
  const {
    data: {
      username,
      created_at,
      bank,
      billing_address,
      shipping_address,
      products,
      shipping_products,
      image,
    },
  } = await axios.get(`${API_URL}/users/${artistId}`);
  return {
    username,
    created_at,
    bank,
    image: image[0]?.url,
    billing_address,
    shipping_address,
    products: products.map(
      ({
        name,
        description,
        price,
        image,
        id,
        like,
        share,
        artist_name,
        product_weight,
        product_height,
        product_length,
        product_width,
        technique,
        country,
      }) => ({
        id,
        name,
        description,
        price,
        imageUrl: image[0].url,
        like,
        share,
        artist_name,
        product_weight,
        product_height,
        product_length,
        product_width,
        technique,
        country,
      })
    ),
    shipping_products: shipping_products.map(
      ({ id, trackno, name, image, status, location }) => ({
        id,
        trackno,
        name,
        imageUrl: image[0].url,
        status,
        location,
      })
    ),
  };
};
