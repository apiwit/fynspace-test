import axios from 'axios';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export const getToS = async () => {
  const {
    data: { tos, appendix_a, privacy },
  } = await axios.get(`${API_URL}/abouts/1`);
  return {
    tos,
    appendix_a,
    privacy,
  };
};

export const getAbout_2 = async () => {
  const {
    data: { cookie, ip, refund },
  } = await axios.get(`${API_URL}/abouts/2`);
  return {
    cookie,
    ip,
    refund,
  };
};

export const getAbout_3 = async () => {
  const {
    data: { shipping, community, content },
  } = await axios.get(`${API_URL}/abouts/3`);
  return {
    shipping,
    community,
    content
  };
};