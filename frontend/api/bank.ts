import axios from 'axios';
import Cookie from 'js-cookie';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:1337';

export const addBankAccount = async ({ name, number, owner }) => {
  const token = Cookie.get('token');
  const response = await axios.post(
    `${API_URL}/banks`,
    {
      name,
      number,
      owner,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
  console.log(`response`, response);
};

export const UpdateBankAccount = async (name: any, number: any, bankId: any) => {
  await axios.put(`${API_URL}/banks/${bankId}`, {
    name: name,
    number: number,
  });
};
