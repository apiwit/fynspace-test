module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: (theme) => ({
        'standard-img': "url('/abstract-art.jpg')",
        'homepage-left':
          "url('https://images.unsplash.com/photo-1567095761054-7a02e69e5c43?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=934&q=80')",
        'homepage-right':
          "url('https://images.unsplash.com/photo-1484860348026-73c1044c9148?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80')",
      }),
      colors: {
        facebook: '#3b5998',
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['group-focus'],
      textColor: ['group-focus'],
      opacity: ['disabled'],
    },
  },
  plugins: [],
};
