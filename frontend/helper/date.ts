export default function isBeforeNow(dateString) {
  const deadline = new Date(dateString);
  const now = new Date();
  const isBeforeNow = deadline.getTime() < now.getTime();
  return isBeforeNow;
}
