'use strict';
const STEP = 10;
/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const omise = require('omise')({
  secretKey: 'skey_test_5lm0wcrg1w1sq6ku8eb',
  omiseVersion: '2015-09-10',
});

module.exports = {
  create: async (ctx) => {
    const { bidder, price, product, tokenId } = ctx.request.body;
    // start auction

    const bids = await strapi.query('bid').find({ product });

    const highest = bids.reduce((max, item) => {
      return Math.max(item.price, max);
    }, 0);

    // check if price is valid if price is under the highest
    if (price < highest + STEP)
      return {
        error: `sorry you have to bid at least ${STEP} more than the current price`,
      };

    // Register the bid in the database
    const bid = await strapi.services.bid.create({
      bidder,
      price,
      product,
    });

    //charge on stripe or something
    const omiseAmount = Math.floor(price * 100);
    const resp = await omise.charges.create({
      description: `Charge for bid (order) ID: ${bid.id}`,
      amount: omiseAmount, // in Satang
      currency: 'thb',
      capture: true,
      card: tokenId,
    });

    await strapi.services.bid.update(
      { id: bid.id },
      {
        charge_id: resp.id,
      }
    );

    if (!resp.captured) {
      return {
        error: `failure at capturing :${resp.failure_message}`,
      };
    }

    // if first bid
    if (bids.length == 0) {
      addBidDeadline(product_id);
    }

    return bid;
  },
};

function addBidDeadline(product_id) {
  strapi.query('product').update(
    { id: product_id },
    {
      bid_end: addDays(new Date(), 1),
    }
  );
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}
